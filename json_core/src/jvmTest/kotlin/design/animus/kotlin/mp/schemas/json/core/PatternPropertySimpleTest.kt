/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core

import design.animus.kotlin.mp.schemas.json.core.interfaces.*
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import org.junit.Test
import kotlin.test.assertTrue

val sampleMapRegex = Regex("""\w{3}-\d{3}""")

class BasePatternMapTest {
  private val logger = KotlinLogging.logger {}

  @Serializable
  data class SampleMap(
    override val map: Map<String, Int>,
    @Transient
    override val regex: Regex = Regex("""\w{3}-\d{3}""")

  ) : IPatternMap<Int> {
    companion object : IPatternMapUtils<Int> {

      override fun of(vararg pairs: Pair<String, Int>): SampleMap {
        return SampleMap(mapOf(*validatePairs(sampleMapRegex, *pairs)))
      }
    }
  }

  @ExperimentalSerializationApi
  @Serializable
  data class Sample(
    val message: String,
    override val schemaAdditionalProperties: Map<String, Int>,
    override val schemaPatternProperties: SampleMap? = null
  ) : IJSONSchemaObjectBase,
    IJSONSchemaObjectBaseWithAdditionalProperties<Int>,
    IJSONSchemaObjectBaseWithPatternProperty<Int> {
    override fun getAJSONSchemaSerializer(): AJSONSchemaSerializer<IJSONSchemaObjectBase> {
      return SampleSerializer as AJSONSchemaSerializer<IJSONSchemaObjectBase>
    }
  }

  @ExperimentalSerializationApi
  object SampleSerializer : AJSONSchemaSerializer<Sample>(Sample.serializer()) {
    override val properties = listOf("message")
    override val additionalProperties = true
    override val patternProperty = true
    override val patternProperties = false
    override val regexes = setOf<Regex>(Regex("""\w{3}-\d{3}"""))
    override val regexToType: Map<Regex, String> = mapOf()
  }

  @ExperimentalSerializationApi
  @Test
  fun baseTest() {
    val data = Sample(
      "Hello",
      schemaAdditionalProperties = mapOf("abc" to 1),
      schemaPatternProperties = SampleMap.of(
        "abc-123" to 1,
        "def-456" to 2
      )
    )
    logger.info { data }
    val rsp = Json.encodeToString(SampleSerializer, data)
    logger.info { rsp }
    val obj = Json.decodeFromString(SampleSerializer, rsp)
    logger.info { obj }
    assertTrue("Serialized then parsed message equal.") { obj.message == data.message }
    assertTrue("Serialized then parsed additionalProperties equal.") { obj.schemaAdditionalProperties == data.schemaAdditionalProperties }
    assertTrue("Serialized then parsed patternProperties MAP equal.") { obj.schemaPatternProperties?.map == data.schemaPatternProperties?.map }
  }

  @ExperimentalSerializationApi
  @Test
  fun propertiesDoNotMatchRegex() {
    val data = Sample(
      "Hello",
      schemaAdditionalProperties = mapOf("abc" to 1),
      schemaPatternProperties = SampleMap.of(
        "abc" to 1,
        "def" to 2
      )
    )
    logger.info { data }
    val rsp = Json.encodeToString(SampleSerializer, data)
    logger.info { rsp }
    val obj = Json.decodeFromString(SampleSerializer, rsp)
    logger.info { obj }
    assertTrue("Returned pattern properties is null") { obj.schemaPatternProperties == null }
  }
}
