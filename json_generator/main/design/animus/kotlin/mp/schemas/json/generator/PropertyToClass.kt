/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator

import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeName
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.containers.*
import design.animus.kotlin.mp.schemas.json.generator.propertyhandler.reference.referenceTypeHandler
import design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions.*
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlin.reflect.KClass
import kotlinx.serialization.json.JsonElement

sealed class TypeResponse {
  object Empty : TypeResponse()
  data class Name<S : Any>(
    val className: TypeName,
    val serializer: Option<out KClass<out S>> = Option.None(),
    val definition: Option<JSONSchemaProperty> = Option.None()
  ) :
    TypeResponse()

  data class Parameterized(val parameterized: ParameterizedTypeName) : TypeResponse()

  suspend fun toTypeName() = when (this) {
    Empty -> ClassName("kotlin", "Unit")
    is Name<*> -> {
      when (val ser = this.serializer) {
        is Option.Some -> this.className.copy(
          annotations = listOf(
            AnnotationSpec.builder(kotlinx.serialization.Serializable::class)
              .addMember("with=%T::class", ser.item)
              .build()
          )
        )
        is Option.None -> this.className
      }
    }
    is Parameterized -> this.parameterized
    Empty -> JsonElement::class.toClassName().copy(nullable = true)
  }
}

suspend fun createInlineObject(
  name: String,
  inObj: PropertyTypes.Containers.Object,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
): TypeResponse.Name<Unit> {
  val packageName = mutatePackageNameIfEnabled("${config.packageBase}.anonymous", name, config)
  val (className, _, _) = mutateNameIfEnabled(name, config)
  val file = fileSpecBootStrap(packageName, className)
  val rsp = when (val objRSP = createObject(name, inObj, config, namePrefix)) {
    is CreateObjectResponse.Object -> {
      val (objSerializer, importList) = createObjectSerializer(name, inObj, config, packageName)
      importList.forEach { (packageName, classNames) ->
        file.addImport(packageName, *classNames.toTypedArray())
      }
      file
        .addType(objRSP.obj)
        .addType(objSerializer)
      TypeResponse.Name<Unit>(
        ClassName(packageName, className)
          .copy(
            annotations = listOf(
              createCustomSerializer(ClassName(packageName, createObjectSerializerName(className)))
            )
          )
      )
    }
    is CreateObjectResponse.Alias -> {
      file.addTypeAlias(objRSP.alias)
      TypeResponse.Name<Unit>(
        ClassName(packageName, className)
      )
    }
  }
  file.build().writeTo(config.outputPath)
  return rsp
}

data class PropertyToClassArgs(
  val name: String,
  val prop: PropertyTypes,
  val config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  val wrapper: UnionWrapper = UnionWrapper.None,
  val wrapperPosition: UnionWrapperPosition = UnionWrapperPosition.External,
  val namePrefix: Option<String> = Option.None()
)

suspend fun propertyToClass(
  name: String,
  prop: PropertyTypes,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  wrapper: UnionWrapper = UnionWrapper.None,
  wrapperPosition: UnionWrapperPosition = UnionWrapperPosition.External,
  namePrefix: Option<String> = Option.None()
): TypeResponse {
  val unionParentName = when (namePrefix) {
    is Option.None -> name
    is Option.Some -> "${namePrefix.item.capitalize()}${name.capitalize()}"
  }
  val propertyToClassArgs = PropertyToClassArgs(
    name = name,
    prop = prop,
    config = config,
    wrapper = wrapper,
    wrapperPosition = wrapperPosition,
    namePrefix = namePrefix

  )
  return when (prop) {
    is PropertyTypes.Property.BasicProperty -> {
      val basic = prop
      val jvmType = basic.property.platformRepresentation.jvmType
      TypeResponse.Name(ClassName(jvmType.packageName, jvmType.className), serializer = jvmType.serializer)
    }
    is PropertyTypes.Property.Reference -> {
      referenceTypeHandler(propertyToClassArgs)
    }
    is PropertyTypes.Property.OneOf -> TypeResponse.Name<Unit>(
      createOneOf(
        unionParentName,
        prop,
        config,
        wrapper,
        wrapperPosition = UnionWrapperPosition.Internal
      )
    )
    is PropertyTypes.Property.AnyOf -> TypeResponse.Name<Unit>(
      createAnyOf(
        unionParentName,
        prop,
        config,
        wrapper,
        wrapperPosition
      )
    )
    is PropertyTypes.Property.AllOf -> TypeResponse.Name<Unit>(
      createAllOf(
        unionParentName,
        prop,
        config,
        wrapper,
        wrapperPosition
      )
    )
    is PropertyTypes.Property.PatternMap -> {
      logger.debug { "Received property patterns of: $prop" }
      if (prop.patterns.isNotEmpty()) {
        val (patternMap, isSealed) = createPatternMap(name, prop.patterns, config, namePrefix = namePrefix)
        if (isSealed) {
          TypeResponse.Parameterized(
            ClassName("kotlin.collections", "Set").parameterizedBy(patternMap)
          )
        } else {
          TypeResponse.Name<Unit>(patternMap)
        }
      } else {
        TypeResponse.Empty
      }
    }
    PropertyTypes.Property.AnyProperty -> TypeResponse.Name<Unit>(
      JsonElement::class.toClassName()
    )
    is PropertyTypes.Containers.Array -> {
      createArray(name, prop, config, namePrefix)
    }
    is PropertyTypes.Containers.Object -> {
      val objName = when (namePrefix) {
        is Option.Some -> "${namePrefix.item.capitalize()}${name.capitalize()}"
        is Option.None -> name.capitalize()
      }
      createInlineObject(objName, prop, config)
    }
    is PropertyTypes.Property.AnonymousObject -> {
      val anonymousName = when (namePrefix) {
        is Option.Some -> "Anonymous${namePrefix.item.capitalize()}${name.capitalize()}"
        is Option.None -> "Anonymous${name.capitalize()}"
      }
      createInlineObject(anonymousName, prop.toObjectContainer(), config, namePrefix = namePrefix)
    }
    is PropertyTypes.Containers.EnumProperty -> {
      TypeResponse.Name<Unit>(createEnum(namePrefix, name, prop, config))
    }
    else -> {
      logger.warn { "Container types are not supported as a property, they should be passed as a reference" }
      TypeResponse.Empty
    }
  }
}
