package design.animus.kotlin.mp.schemas.json.diff

import kotlinx.serialization.Serializable

@Serializable
data class Changes<V : Any?>(
  val action: Actions,
  val path: String,
  val old: V,
  val new: V
)
