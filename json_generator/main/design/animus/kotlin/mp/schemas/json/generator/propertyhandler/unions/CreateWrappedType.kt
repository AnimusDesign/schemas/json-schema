package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeName
import design.animus.kotlin.mp.schemas.json.generator.TypeResponse

internal suspend fun createWrappedType(
  typeRSP: TypeResponse,
  wrapper: UnionWrapper,
  wrapperPosition: UnionWrapperPosition
): TypeName {
  val rawType = typeRSP.toTypeName()
  return if (wrapper is UnionWrapper.None || wrapperPosition is UnionWrapperPosition.External) {
    rawType
  } else if (wrapperPosition is UnionWrapperPosition.Internal && wrapper !is UnionWrapper.None) {
    when (wrapper) {
      UnionWrapper.PropertyMap -> {
        wrapper.wrapperClass.parameterizedBy(ClassName("kotlin", "String"), rawType)
      }
      UnionWrapper.PropertyArray -> {
        wrapper.wrapperClass.parameterizedBy(rawType)
      }
      else -> rawType
    }
  } else {
    rawType
  }
}
