/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

enum class SchemaTypes(val friendly: String) {
  OBJECT("object"),
  ARRAY("array"),
  BOOLEAN("boolean")
}

suspend fun String.toSchemaTypes() = when (this) {
  "object" -> SchemaTypes.OBJECT
  "array" -> SchemaTypes.ARRAY
  "boolean" -> SchemaTypes.BOOLEAN
  else -> throw Exception("Unknown Schema Type.")
}

class SchemaTypeSerializer : KSerializer<SchemaTypes> {
  override val descriptor: SerialDescriptor
    get() = PrimitiveSerialDescriptor("SchemaTypeSerializer", PrimitiveKind.STRING)

  override fun deserialize(decoder: Decoder): SchemaTypes {
    val rawType = decoder.decodeString()
    val castType = SchemaTypes.values().firstOrNull { it.friendly == rawType }
    return castType ?: throw Exception("The provided type of $rawType does not match object or array.")
  }

  override fun serialize(encoder: Encoder, value: SchemaTypes) {
    encoder.encodeString(value.friendly)
  }
}
