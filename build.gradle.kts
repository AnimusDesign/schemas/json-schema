/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.net.URI

buildscript {
    repositories {
        mavenLocal()
        maven { url = uri("https://plugins.gradle.org/m2/") }
        maven { url = uri("https://animusdesign-repository.appspot.com") }
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx") }
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.32")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:9.4.0")
        classpath("com.github.jengelman.gradle.plugins:shadow:6.1.0")
        classpath("org.jetbrains.kotlin:kotlin-serialization:1.4.32")
        classpath("net.researchgate:gradle-release:2.8.1")
    }
}

allprojects {
    repositories {
        mavenLocal()
        jcenter()
        mavenCentral()
        maven { url = uri("https://animusdesign-repository.appspot.com") }
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://oss.jfrog.org/oss-release-local") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx/") }
    }
}

subprojects {
    if (File("${this.projectDir}/build.gradle.kts").isFile) {
        if (project.path.contains("json_core") || project.path.contains("json_diff")) {
            apply(plugin = "org.jetbrains.kotlin.multiplatform")
        } else {
            apply(plugin = "org.jetbrains.kotlin.jvm")
        }
        apply(plugin = "org.jetbrains.kotlin.plugin.serialization")
        apply(plugin = "maven-publish")
        apply(plugin = "org.jlleitschuh.gradle.ktlint")
        apply(plugin = "net.researchgate.release")
        configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
            debug.set(true)
            verbose.set(true)
            android.set(false)
            enableExperimentalRules.set(true)
            additionalEditorconfigFile.set(file("$rootDir/.editorconfig"))
            filter {
                include("**/main/**", "**/src/main/**", "**/test/**", "**/src/test/**")
                exclude("**/generated/**", "generated")
            }
        }
        val version: String by project
        val baseGroup = "design.animus.kotlin.mp.schemas"
        group = baseGroup
        this.version = version
        configure<PublishingExtension> {
            repositories {
                maven {
                    url = URI("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven/")
                    credentials(HttpHeaderCredentials::class.java) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }
        }
    }
}
