/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.jsonschema

import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.platform.JSONSchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class OpenAPITest : ISchemaTestBase {
  @Test
  fun testRead() = runBlocking {
    val content = loadTestSchema("openapi-3.0.schema.json")
    val unsureSchema = JSONSchemaParser.parseJSON(content)

    assertTrue("Schema is of json object") { unsureSchema is JSONSchemaObject }
    val schema = unsureSchema as JSONSchemaObject

    assertTrue("Schema description matches") {
      schema.description == "Validation schema for OpenAPI Specification 3.0.X."
    }

    assertTrue {
      schema.required is PropertyTypes.Property.Required.ListOfStrings &&
        (schema.required as PropertyTypes.Property.Required.ListOfStrings).required.containsAll(
          listOf(
            "openapi",
            "info",
            "paths"
          )
        )
    }

    assertTrue("Schema has parsed the anticipated properties.") {
      schema.properties.map { it.propertyName!! }.containsAll(
        listOf(
          "openapi",
          "info",
          "externalDocs",
          "servers",
          "security",
          "tags",
          "paths",
          "components"
        )
      )
    }

    val openAPIProp = schema.properties.firstOrNull { it.propertyName == "openapi" }
      ?: error("Could not find openapi property in schema")
    assertTrue("schema/properties/openapi is of Basic Property") {
      (openAPIProp.type is PropertyTypes.Property.BasicProperty) &&
        (openAPIProp.type as PropertyTypes.Property.BasicProperty).property == JSONSchemaTypes.PlainString
    }

    val serversProp = schema.properties.firstOrNull { it.propertyName == "servers" }
      ?: error("Could not find servers property in schema")
    assertTrue("Srvers property is an array type") {
      serversProp.type is PropertyTypes.Containers.Array
    }
    val serversPropArray = serversProp.type as PropertyTypes.Containers.Array
    assertTrue {
      serversPropArray.items is PropertyTypes.Property.Reference &&
        (serversPropArray.items as PropertyTypes.Property.Reference).reference == "#/definitions/Server"
    }

    val serversDefinition = schema.definitions.firstOrNull { it.propertyName == "Server" }
      ?: error("Could not find definition for server.")
    assertTrue("Server definition is an object.") {
      serversDefinition.type is PropertyTypes.Containers.Object
    }
    val serverDefObj = serversDefinition.type as PropertyTypes.Containers.Object
    assertTrue("Server definition does not have additional properties") { serverDefObj.additionalProperties.isNone() }
    assertTrue("Server definition required properties matches.") {
      serverDefObj.required.isSome() &&
        (serverDefObj.required.forceSome() is PropertyTypes.Property.Required.ListOfStrings) &&
        (serverDefObj.required.forceSome() as PropertyTypes.Property.Required.ListOfStrings).required.containsAll(
          listOf("url")
        )
    }
  }
}
