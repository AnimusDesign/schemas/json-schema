/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator

// suspend fun parseType(type:PropertyTypes) =                    when (type) {
//    is PropertyTypes.BasicProperty -> {
//        val basicProp = type as PropertyTypes.BasicProperty
//        val rawClass = basicProp.property.platformRepresentation.commonType
//        ClassName(rawClass.packageName, rawClass.className)
//    }
//    is PropertyTypes.Object -> createObject((type as PropertyTypes.Object).properties)
// //                        is PropertyTypes.ObjectWithAdditionalProperties -> TODO()
// //                        is PropertyTypes.Array -> TODO()
// //                        is PropertyTypes.EnumProperty -> TODO()
// //                        is PropertyTypes.Reference -> TODO()
// //                        is PropertyTypes.OneOf -> TODO()
// //                        is PropertyTypes.AnyOf -> TODO()
// //                        is PropertyTypes.AllOf -> TODO()
// //                        is PropertyTypes.PatternMap -> TODO()
// //                        PropertyTypes.AnyProperty -> TODO()
// }
