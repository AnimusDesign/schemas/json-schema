package design.animus.kotlin.mp.schemas.json.diff

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import kotlin.test.assertTrue
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import org.junit.Test

@ExperimentalSerializationApi
class TestRecursive {

  @Serializable
  data class Fruit(
    val fruit: String,
    val price: Int
  )

  @Serializable
  data class Sample(
    val message: String,
    val fruit: Fruit? = null
  ) : IJSONSchemaObjectBase {
    override fun getAJSONSchemaSerializer(): AJSONSchemaSerializer<IJSONSchemaObjectBase> {
      return SampleSerializer as AJSONSchemaSerializer<IJSONSchemaObjectBase>
    }
  }

  object SampleSerializer : AJSONSchemaSerializer<Sample>(Sample.serializer()) {
    override val properties = listOf("message", "fruit")
    override val additionalProperties = true
    override val patternProperty = false
    override val patternProperties = true
    override val regexes = setOf<Regex>()
    override val regexToType = mapOf<Regex, String>()
  }

  @Serializable
  data class SampleOfList(
    val message: String,
    val fruit: List<Fruit> = listOf()
  ) : IJSONSchemaObjectBase {
    override fun getAJSONSchemaSerializer(): AJSONSchemaSerializer<IJSONSchemaObjectBase> {
      return SampleOfListSerializer as AJSONSchemaSerializer<IJSONSchemaObjectBase>
    }
  }

  object SampleOfListSerializer : AJSONSchemaSerializer<SampleOfList>(SampleOfList.serializer()) {
    override val properties = listOf("message", "fruit")
    override val additionalProperties = true
    override val patternProperty = false
    override val patternProperties = true
    override val regexes = setOf<Regex>()
    override val regexToType = mapOf<Regex, String>()
  }

  @Test
  fun testUpdate() = runBlocking {
    val old = Sample(message = "Hello")
    val new = Sample(message = "Goodbye")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    assertTrue("There is one change") { changes.size == 1 }
    val change = changes.first()
    assertTrue("Change action is update") { change.action == Actions.Update }
    assertTrue("Change path is 'message'") { change.path == "message" }
    assertTrue("Change old matches old struct") { change.old == old.message }
    assertTrue("Change new matches new struct") { change.new == new.message }
  }

  @Test
  fun testRecursiveCreate() = runBlocking {
    val old = Sample(message = "Hello")
    val new = Sample(message = "Hello", fruit = Fruit("banana", 1))
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    println(changes)
    assertTrue("There are two changes") { changes.size == 2 }
    val fruitChange = changes.first { it.path == "fruit/fruit" }
    assertTrue("Change action is create") { fruitChange.action == Actions.Create }
    assertTrue("Change path is 'fruit/fruit'") { fruitChange.path == "fruit/fruit" }
    assertTrue("Change old matches old struct") { fruitChange.old == null }
    assertTrue("Change new matches new struct") { fruitChange.new == new.fruit?.fruit }

    val priceChange = changes.first { it.path == "fruit/price" }
    assertTrue("Change action is create") { priceChange.action == Actions.Create }
    assertTrue("Change path is 'fruit/fruit'") { priceChange.path == "fruit/price" }
    assertTrue("Change old matches old struct") { priceChange.old == null }
    assertTrue("Change new matches new struct") { priceChange.new == new.fruit?.price?.toString() }
  }

  @Test
  fun testUpdateOfList() = runBlocking {
    val old = SampleOfList(message = "Hello")
    val new = SampleOfList(message = "Goodbye")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    assertTrue("There is one change") { changes.size == 1 }
    val change = changes.first()
    assertTrue("Change action is update") { change.action == Actions.Update }
    assertTrue("Change path is 'message'") { change.path == "message" }
    assertTrue("Change old matches old struct") { change.old == old.message }
    assertTrue("Change new matches new struct") { change.new == new.message }
  }

  @Test
  fun testRecursiveCreateOfList() = runBlocking {
    val old = SampleOfList(message = "Hello")
    val new = SampleOfList(message = "Hello", fruit = listOf(Fruit("banana", 1)))
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    println(changes)
    assertTrue("There are two changes") { changes.size == 2 }
    val fruitChange = changes.first { it.path == "fruit/index:0/fruit" }
    assertTrue("Change action is create") { fruitChange.action == Actions.Create }
    assertTrue("Change path is 'fruit/fruit'") { fruitChange.path == "fruit/index:0/fruit" }
    assertTrue("Change old matches old struct") { fruitChange.old == null }
    assertTrue("Change new matches new struct") { fruitChange.new == new.fruit.first().fruit }

    val priceChange = changes.first { it.path == "fruit/index:0/price" }
    assertTrue("Change action is create") { priceChange.action == Actions.Create }
    assertTrue("Change path is 'fruit/fruit'") { priceChange.path == "fruit/index:0/price" }
    assertTrue("Change old matches old struct") { priceChange.old == null }
    assertTrue("Change new matches new struct") { priceChange.new == new.fruit.first().price.toString() }
  }

  @Test
  fun testRecursiveDeleteOfList() = runBlocking {
    val old = SampleOfList(message = "Hello", fruit = listOf(Fruit("banana", 1)))
    val new = SampleOfList(message = "Hello")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    println(changes)
    assertTrue("There are two changes") { changes.size == 2 }
    val fruitChange = changes.first { it.path == "fruit/index:0/fruit" }
    assertTrue("Change action is create") { fruitChange.action == Actions.Delete }
    assertTrue("Change path is 'fruit/fruit'") { fruitChange.path == "fruit/index:0/fruit" }
    assertTrue("Change old matches old struct") { fruitChange.new == null }
    assertTrue("Change new matches new struct") { fruitChange.old == old.fruit.first().fruit }

    val priceChange = changes.first { it.path == "fruit/index:0/price" }
    assertTrue("Change action is create") { priceChange.action == Actions.Delete }
    assertTrue("Change path is 'fruit/fruit'") { priceChange.path == "fruit/index:0/price" }
    assertTrue("Change old matches old struct") { priceChange.new == null }
    assertTrue("Change new matches new struct") { priceChange.old == old.fruit.first().price.toString() }
  }

  @Test
  fun testRecursiveCreateOfListPlural() = runBlocking {
    val old = SampleOfList(message = "Hello", fruit = listOf(Fruit("banana", 1), Fruit("Apple", 2)))
    val new = SampleOfList(message = "Hello")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    println(changes)
    assertTrue("There are two changes") { changes.size == 4 }
    val fruitChange = changes.first { it.path == "fruit/index:0/fruit" }
    assertTrue("Change action is create") { fruitChange.action == Actions.Delete }
    assertTrue("Change path is 'fruit/fruit'") { fruitChange.path == "fruit/index:0/fruit" }
    assertTrue("Change old matches old struct") { fruitChange.new == null }
    assertTrue("Change new matches new struct") { fruitChange.old == old.fruit.first().fruit }

    val priceChange = changes.first { it.path == "fruit/index:0/price" }
    assertTrue("Change action is create") { priceChange.action == Actions.Delete }
    assertTrue("Change path is 'fruit/fruit'") { priceChange.path == "fruit/index:0/price" }
    assertTrue("Change old matches old struct") { priceChange.new == null }
    assertTrue("Change new matches new struct") { priceChange.old == old.fruit.first().price.toString() }

    val appleFruitChange = changes.first { it.path == "fruit/index:1/fruit" }
    assertTrue("Change action is create") { appleFruitChange.action == Actions.Delete }
    assertTrue("Change path is 'fruit/fruit'") { appleFruitChange.path == "fruit/index:1/fruit" }
    assertTrue("Change old matches old struct") { appleFruitChange.new == null }
    assertTrue("Change new matches new struct") { appleFruitChange.old == old.fruit[1].fruit }

    val applePriceChange = changes.first { it.path == "fruit/index:1/price" }
    assertTrue("Change action is create") { applePriceChange.action == Actions.Delete }
    assertTrue("Change path is 'fruit/fruit'") { applePriceChange.path == "fruit/index:1/price" }
    assertTrue("Change old matches old struct") { applePriceChange.new == null }
    assertTrue("Change new matches new struct") { applePriceChange.old == old.fruit[1].price.toString() }
  }
}
