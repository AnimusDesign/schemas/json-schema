/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.schema.*
import design.animus.kotlin.mp.schemas.json.parser.schema.drafts.SchemaVersions
import design.animus.kotlin.mp.schemas.json.parser.schema.drafts.determineJsonSchemaVersion
import design.animus.kotlin.mp.schemas.json.parser.schema.drafts.v4.RawJSONSchemaV4
import design.animus.kotlin.mp.schemas.json.parser.schema.drafts.v7.RawJSONSchemaV7
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Required
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.castToPropertyType
import design.animus.kotlin.mp.schemas.json.parser.schema.utils.PropertyUtils
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import java.io.File
import java.nio.file.Path
import kotlin.system.exitProcess

internal val logger = KotlinLogging.logger { }

object JSONSchemaParser {
  private val json = Json {
    isLenient = true
    ignoreUnknownKeys = true
  }

  private suspend fun buildJSONObjectSchema(schemaTypes: SchemaTypes, rawSchema: RawJSONSchema): JSONSchema {
    val definitions = rawSchema.definitions.map { (name, schema) ->
      logger.info { "Attempting to make definition for: $name of: $schema" }
      JSONSchemaProperty(
        propertyName = name,
        type = PropertyUtils.getPropertyType(schema.type, schema.format, schema, listOf())
      )
    }
    val addProps = rawSchema.additionalProperties?.castToPropertyType(definitions)
    val patternProps = rawSchema.patternProperties.castToPropertyType(definitions)
    logger.debug { rawSchema.properties }
    return JSONSchemaObject(
      id = rawSchema.id,
      schema = rawSchema.schema,
      type = schemaTypes,
      description = rawSchema.description,
      properties = rawSchema.properties.map { (name, inSchema) ->
        val propType = when (inSchema) {
          is RawProperty.RawOfJsonSchemaProperty -> {
            val schema = inSchema.item
            PropertyUtils.getPropertyType(schema.type, schema.format, schema, definitions)
          }
          is RawProperty.RawOfBoolean, RawProperty.RawOfEmpty -> PropertyTypes.Property.AnyProperty
        }
        JSONSchemaProperty(
          propertyName = name,
          type = propType
        )
      },
      additionalProperties = if (addProps == null) Option.None() else Option.Some(addProps),
      patternProperties = patternProps,
      required = when (rawSchema.required) {
        is Required.RequiredReference -> PropertyTypes.Property.Required.Reference(rawSchema.required.value.ref!!)
        is Required.RequiredStrings -> PropertyTypes.Property.Required.ListOfStrings(rawSchema.required.value)
        Required.Empty -> PropertyTypes.Property.Required.ListOfStrings(listOf())
      },
      dependencies = rawSchema.dependencies,
      definitions = definitions
    )
  }

  private suspend fun buildJSONArraySchema(rawSchema: RawJSONSchema): JSONSchema {
    TODO()
  }

  suspend fun schemaTypeDirector(schemaTypes: SchemaTypes, rawSchema: RawJSONSchema): JSONSchema? {
    return when (schemaTypes) {
      SchemaTypes.OBJECT -> buildJSONObjectSchema(schemaTypes, rawSchema)
      SchemaTypes.ARRAY -> buildJSONArraySchema(rawSchema)
      SchemaTypes.BOOLEAN -> null
    }
  }

  suspend fun parseJSON(content: String): JSONSchema {
    return try {
      val temp = when (determineJsonSchemaVersion(content)) {
        SchemaVersions.V4 -> json.decodeFromString(RawJSONSchemaV4.serializer(), content)
        SchemaVersions.V7 -> json.decodeFromString(RawJSONSchemaV7.serializer(), content)
      }
      temp.toJsonSchema()
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in parseJson: ${e.message}" }
      e.printStackTrace()
      exitProcess(1)
    }
  }

  suspend fun readFromFile(path: Path): JSONSchema {
    logger.debug { "Received a file path of: $path to parse." }
    val fileContent = File(path.toUri()).readText()
    logger.debug { "Filed loaded successfully." }
    return parseJSON(fileContent)
  }
}
