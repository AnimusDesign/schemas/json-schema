/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.reference

import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.containers.createObjectSerializerName
import design.animus.kotlin.mp.schemas.json.generator.externalReferences
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.util.*
import java.io.File
import kotlin.jvm.internal.MutablePropertyReference1
import kotlin.jvm.internal.MutablePropertyReference1Impl
import kotlin.system.exitProcess
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import okhttp3.Cache
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.Request

@KtorExperimentalAPI
suspend fun externalReferenceHandler(args: PropertyToClassArgs): TypeResponse {
  val prop = args.prop as PropertyTypes.Property.Reference
  return try {
    if (prop.reference.contains("http")) {
      logger.debug { "Found an external reference based off of an http url of: ${prop.reference}" }
      if (externalReferences.containsKey(prop.reference)) {
        logger.debug { "Reference already created returning created external reference." }
        TypeResponse.Name(
          externalReferences[prop.reference]
            ?: error("Prop reference of ${prop.reference} is in externalReferences but not defined.")
        )
      } else {
        // Needed to ensure proper ktor dependencies as a gradle plugin
        MutablePropertyReference1Impl::class
        MutablePropertyReference1::class
        HttpClientConfig::class
        try {
          val client = OkHttpClient.Builder()
            .cache(
              Cache(
                directory = File("/tmp/ok-http.cache"),
                maxSize = 250L * 1024L * 1024L // 250MB
              )
            )
            .connectionSpecs(
              listOf(
                ConnectionSpec.MODERN_TLS,
                ConnectionSpec.COMPATIBLE_TLS,
                ConnectionSpec.CLEARTEXT
              )
            )
            .build()
          val rawName = prop.reference.replace("#", "").split("/").last()
          val packageBase = "${args.config.packageBase}.foreign.$rawName".toLowerCase()
          logger.debug { "Determined raw name as: $rawName" }
          val (refName, _, _) = rawName.sanitizeForClassName()
          logger.debug { "Cleaned name too: $refName" }

          logger.debug { "Attempting to make call too: ${prop.reference}" }
          val req = Request.Builder()
            .url(prop.reference)
            .build()
          client.newCall(req).execute().use { response ->
            if (!response.isSuccessful) error("Failed to retrieve external reference, $response")
            val body = response.body ?: error("Response body is empty")
            val nestedConfig = args.config.copy(
              packageBase = packageBase,
              schemaFile = File("/tmp/$refName"),
              schemaName = refName
            )
            logger.debug { "Created a nested configuration of: $nestedConfig" }
            runBlocking {
              JSONSchemaGeneratorExecutor.initiate(body.string(), nestedConfig)
            }
          }
          val typeName = ClassName(packageBase, refName)
            .copy(
              annotations = listOf(
                AnnotationSpec.builder(Serializable::class)
                  .addMember(
                    "with=%T::class",
                    ClassName(packageBase, createObjectSerializerName(refName))
                  )
                  .build()
              )
            )
          externalReferences[prop.reference] = typeName
          TypeResponse.Name<Unit>(typeName)
        } catch (e: ClientRequestException) {
          logger.warn { "Failed to retrieve: ${prop.reference} with error: ${e.message}" }
          val t = JsonElement::class.toClassName()
          TypeResponse.Name<Unit>(t)
        }
      }
    } else {
      TypeResponse.Name<Unit>(JsonElement::class.toClassName())
    }
  } catch (e: Exception) {
    logger.error { "Encountered an unknown exception in external reference ${e.message}" }
    e.printStackTrace()
    exitProcess(1)
  }
}
