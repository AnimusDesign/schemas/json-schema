package design.animus.kotlin.mp.schemas.json.test.simple

import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.JSONSchemaGeneratorExecutor
import java.io.File

suspend fun main() {
  val schema =
    "/DockerData/projector/intellij-personal/dev/schemas/gitlab/gitlab/src/commonMain/resources/gitlab-ci.schema.json"
  val config = GeneratorConfig(
    packageBase = "design.animus.kotlin.mp.schemas.gitlab.generated",
    outputPath = File("/DockerData/projector/intellij-personal/dev/schemas/gitlab/gitlab/generated/commonMain/kotlin"),
    schemaFile = File(schema),
    schemaName = "GitLab"
  )
  JSONSchemaGeneratorExecutor.generate(schema, config)
}
