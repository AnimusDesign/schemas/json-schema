/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.reference

import design.animus.kotlin.mp.schemas.json.generator.PropertyToClassArgs
import design.animus.kotlin.mp.schemas.json.generator.TypeResponse
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.ReferenceType

suspend fun referenceTypeHandler(args: PropertyToClassArgs): TypeResponse {
  val refProp = args.prop as PropertyTypes.Property.Reference
  return when (refProp.referenceType) {
    ReferenceType.INTERNAL -> internalReferenceHandler(args)
    ReferenceType.EXTERNAL -> externalReferenceHandler(args)
  }
}
