/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

sealed class PrimitiveIntOrProperty {
  data class PrimitiveInt(val value: Int) : PrimitiveIntOrProperty()
  data class Property(val property: RawJSONSchemaProperty) : PrimitiveIntOrProperty()
  object Empty : PrimitiveIntOrProperty()

  @Serializer(forClass = PrimitiveIntOrProperty::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<PrimitiveIntOrProperty> {

    override fun serialize(encoder: Encoder, value: PrimitiveIntOrProperty) = when (value) {
      is PrimitiveInt -> encoder.encodeSerializableValue(Int.serializer(), value.value)
      is Property -> encoder.encodeSerializableValue(RawJSONSchemaProperty.serializer(), value.property)
      is Empty -> encoder.encodeNull()
    }

    override fun deserialize(decoder: Decoder): PrimitiveIntOrProperty = try {
      logger.debug { "In Primitive Int or Property deserialize" }
      val possibleObject = try {
        val value = decoder.decodeInt()
        PrimitiveInt(value)
      } catch (e: Exception) {
        logger.debug { "Value is not an int" }
        null
      }
      val possibleProperty = try {
        Property(decoder.decodeSerializableValue(RawJSONSchemaProperty.serializer()))
      } catch (e: Exception) {
        logger.debug { "Value is not a property" }
        null
      }
      possibleObject ?: possibleProperty ?: Empty
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in Primitive Int Or Property deserialize of: ${e.message}" }
      Empty
    }
  }
}
