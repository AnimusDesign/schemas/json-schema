/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawProperty
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class ArrayItems {
  @Serializable
  data class ListItems(val items: List<RawProperty>) : ArrayItems()

  @Serializable
  data class ObjectItems(val items: RawProperty) : ArrayItems()

  @Serializable
  data class BooleanItems(val items: Boolean) : ArrayItems()

  @Serializable
  object Empty : ArrayItems()

  @ExperimentalSerializationApi
  @Serializer(forClass = ArrayItems::class)
  companion object : KSerializer<ArrayItems> {
    override fun serialize(encoder: Encoder, value: ArrayItems) {
      TODO()
    }

    override fun deserialize(decoder: Decoder): ArrayItems {
      return try {
        logger.debug { "In deserialize array items" }
        val possibleList = try {
          ListItems(decoder.decodeSerializableValue(ListSerializer(RawProperty.serializer())))
        } catch (e: Exception) {
          logger.debug { "Not List items" }
          null
        }
        val possibleObject = try {
          ObjectItems(decoder.decodeSerializableValue(RawProperty.serializer()))
        } catch (e: Exception) {
          logger.debug { "not object items" }
          null
        }
        val possibleBool = try {
          BooleanItems(decoder.decodeBoolean())
        } catch (e: Exception) {
          logger.debug { "not boolean items" }
          null
        }
        when {
          possibleList != null -> {
            possibleList
          }
          possibleObject != null -> {
            possibleObject
          }
          possibleBool != null -> {
            possibleBool
          }
          else -> {
            Empty
          }
        }
      } catch (e: Exception) {
        logger.error { "Encountered an unexpected exception in ArrayItems: ${e.message}" }
        Empty
      }
    }
  }
}
