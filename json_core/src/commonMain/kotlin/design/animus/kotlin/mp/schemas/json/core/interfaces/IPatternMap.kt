/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core.interfaces

interface IPatternMap<V> {
  val map: Map<String, V>
  val regex: Regex
}

interface IPatternMapUtils<V> {

  fun of(vararg pairs: Pair<String, V>): IPatternMap<V>

  fun validatePairs(regex: Regex, vararg pairs: Pair<String, V>) = pairs.filter { (k, _) ->
    regex.matches(k)
  }.toTypedArray()
}
