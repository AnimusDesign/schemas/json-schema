/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.contract

object Versions {
    object Plugins {
        const val kotlin = Dependencies.kotlin
        const val gradlePluginPublish = "0.10.1"
    }

    object Dependencies {
        const val kotlin = "1.4.32"
        const val serialization = "1.1.0"
        const val coroutine = "1.4.3"
        const val junit = "4.12"
        const val kotlinPoet = "1.7.2"
        const val kotlinLogging = "2.0.4"
        const val logback = "1.2.3"
        const val MPDataTypes = "0.1.3"
        const val joda = "2.10.5"
        const val mpUUID = "0.2.3"
        const val mpDate = "0.1.1"
        const val ktor = "1.4.2"
        const val okHTTP = "4.9.1"
    }
}
