package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions

import com.squareup.kotlinpoet.ClassName

interface IUnionWrapper {
  val wrapperClass: ClassName
}

sealed class UnionWrapper : IUnionWrapper {
  object PropertyMap : UnionWrapper() {
    override val wrapperClass = ClassName("kotlin.collections", "Map")
  }

  object PropertyArray : UnionWrapper() {
    override val wrapperClass = ClassName("kotlin.collections", "List")
  }

  object None : UnionWrapper() {
    override val wrapperClass = ClassName("kotlin", "Any")
  }
}

sealed class UnionWrapperPosition {
  object Internal : UnionWrapperPosition()
  object External : UnionWrapperPosition()
}
