/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.additional

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBaseWithAdditionalProperties
import design.animus.kotlin.mp.schemas.json.core.interfaces.additionalPropertiesKey
import design.animus.kotlin.mp.schemas.json.generator.records.ObjectResponse
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes

suspend fun additionalPropertiesBase(
  schema: PropertyTypes.Containers.Object,
  type: PropertyTypes,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  block: suspend () -> Option<TypeName>
): Option<ObjectResponse> {
  val rawType = block()
  val onlyAdditionalProps = schema.properties.isSome() || schema.pattern.isSome()
  return if (rawType.isSome()) {
    val propType = rawType.forceSome()
    val cast = ClassName("kotlin.collections", "Map")
      .parameterizedBy(
        ClassName("kotlin", "String"),
        propType
      )
    val baseParam = ParameterSpec.builder(additionalPropertiesKey, cast)
    if (onlyAdditionalProps) baseParam.defaultValue("%L", "mapOf()")
    Option.Some(
      ObjectResponse(
        properties = listOf(
          PropertySpec.builder(additionalPropertiesKey, cast)
            .addModifiers(KModifier.OVERRIDE)
            .initializer(additionalPropertiesKey)
        ),
        parameters = listOf(baseParam),
        superInterface = listOf(
          IJSONSchemaObjectBaseWithAdditionalProperties::class.toClassName()
            .parameterizedBy(propType)
        )
      )
    )
  } else {
    Option.None()
  }
}
