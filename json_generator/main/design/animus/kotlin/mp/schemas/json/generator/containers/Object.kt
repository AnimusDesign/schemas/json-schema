/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.core.interfaces.*
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import design.animus.kotlin.mp.schemas.json.generator.propertyhandler.additional.additionalPropertiesOfAny
import design.animus.kotlin.mp.schemas.json.generator.propertyhandler.additional.additionalPropertiesOfDefinition
import design.animus.kotlin.mp.schemas.json.generator.utils.getSchemaInterfaceClassName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.serialization.Serializable
import kotlin.system.exitProcess
import kotlinx.serialization.json.JsonElement

sealed class CreateObjectResponse {
  data class Object(val obj: TypeSpec) : CreateObjectResponse()
  data class Alias(val alias: TypeAliasSpec) : CreateObjectResponse()
}

suspend fun createObject(
  name: String,
  schema: PropertyTypes.Containers.Object,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
) = try {
  logger.debug { "Attempting to build class $name" }
  logger.debug { "Schema is: $schema" }
  val (className, requireSerialName, serialName) = mutateNameIfEnabled(name, config)
  /**
   * If we receive an empty object/item definition
   * Set to Map<String, JsonElement>
   */
  if (schema.isEmpty()) {
    logger.debug { "Schema definition is empty:" }
    val aliasType = Map::class.toClassName().parameterizedBy(
      String::class.toClassName(),
      JsonElement::class.toClassName()
    )
    val alias = TypeAliasSpec.builder(className, aliasType)
      .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
      .build()
    logger.debug { "Finished build alias from schema" }
    CreateObjectResponse.Alias(alias)
  } else {
    logger.debug { "Schema is not empty creating object." }
    val obj = TypeSpec.classBuilder(className)
      .addSuperinterface(config.getSchemaInterfaceClassName())
      .addSuperinterface(IJSONSchemaObjectBase::class)
      .addModifiers(KModifier.DATA)
      .addAnnotation(Serializable::class)
      .addAnnotation(experimentalSerializationClass)
      .addFunction(
        FunSpec.builder("getAJSONSchemaSerializer")
          .addModifiers(KModifier.OVERRIDE)
          .returns(
            AJSONSchemaSerializer::class.toClassName().parameterizedBy(
              IJSONSchemaObjectBase::class.toClassName()
            )
          )
          .addCode("return ${createObjectSerializerName(className)} as AJSONSchemaSerializer<IJSONSchemaObjectBase>")
          .build()
      )
    if (requireSerialName) obj.addAnnotation(
      createSerialNameAnnotation(serialName)
    )
    val requiredProps = if (schema.required.isSome()) {
      when (val requiredDef = schema.required.forceSome()) {
        is PropertyTypes.Property.Required.Reference -> listOf()
        is PropertyTypes.Property.Required.ListOfStrings -> requiredDef.required
      }
    } else {
      listOf()
    }
    logger.debug { "Required properties are: $requiredProps" }
    val objProps = mutableListOf<PropertySpec>()
    val objParams = mutableListOf<ParameterSpec>()
    val nestedNamePrefix = when (namePrefix) {
      is Option.None -> Option.Some(name)
      is Option.Some -> Option.Some("${namePrefix.item.capitalize()}${name.capitalize()}")
    }
    logger.debug { "Set nested name prefix: $nestedNamePrefix" }
    if (schema.properties.isSome()) {
      logger.debug { "Object schema has ${schema.properties.forceSome().size} properties" }
      objProps.addAll(
        createObjPropertySpec(
          schema.properties.forceSome(),
          requiredProps,
          config,
          namePrefix = nestedNamePrefix
        )
      )
      objParams.addAll(
        createObjParameterSpec(
          schema.properties.forceSome(),
          requiredProps,
          config,
          namePrefix = nestedNamePrefix
        )
      )
    }
    if (schema.pattern.isSome()) {
      logger.debug { "Object schema has ${schema.pattern.forceSome().size} pattern properties" }
      val key = schemaPatternPropertiesKey
      val patterns = schema.pattern.forceSome()
      val (rawType, isSealed, paramType) = createPatternMap(name, patterns, config)
      val parent = if (isSealed) {
        IJSONSchemaObjectBaseWithPatternProperties::class
          .toClassName()
      } else {
        IJSONSchemaObjectBaseWithPatternProperty::class
          .toClassName()
      }.parameterizedBy(paramType)
      val onlyPatternProperties = schema.additionalProperties.isSome() || schema.properties.isSome()
      val finalType = if (isSealed) rawType else rawType.copy(nullable = onlyPatternProperties)
      obj.addSuperinterface(parent)
      objProps.add(
        PropertySpec.builder(key, finalType)
          .addModifiers(KModifier.OVERRIDE)
          .initializer(key)
          .build()
      )
      val baseParam = ParameterSpec.builder(key, finalType)
      if (onlyPatternProperties) baseParam.defaultValue("%L", if (isSealed) "setOf()" else "null")
      objParams.add(baseParam.build())
    }
    if (schema.additionalProperties.isSome()) {
      val key = additionalPropertiesKey
      val possibleObjResponse = when (schema.additionalProperties.forceSome()) {
        is PropertyTypes.Property.AnyProperty -> additionalPropertiesOfAny(
          schema,
          schema.additionalProperties.forceSome(),
          nestedNamePrefix,
          config
        )
        else -> additionalPropertiesOfDefinition(
          schema,
          schema.additionalProperties.forceSome(),
          nestedNamePrefix,
          config
        )
      }
      if (possibleObjResponse.isSome()) {
        val objResponse = possibleObjResponse.forceSome()
        objProps.addAll(objResponse.properties.map { it.build() })
        objParams.addAll(objResponse.parameters.map { it.build() })
        objResponse.superInterface.forEach { inter ->
          obj.addSuperinterface(inter)
        }
      }
    }
    obj
      .primaryConstructor(
        FunSpec.constructorBuilder()
          .addParameters(objParams.toSet())
          .build()
      )
      .addProperties(objProps.toSet())
    logger.debug { "Finished build object from schema" }
    CreateObjectResponse.Object(obj.build())
  }
} catch (e: Exception) {
  logger.error(e) { "Encountered an unexpected exception while creating object: $name, error of: ${e.message}" }
  exitProcess(1)
}
