/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import design.animus.kotlin.mp.schemas.json.generator.logger
import design.animus.kotlin.mp.schemas.json.generator.mutateNameIfEnabled
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.utils.createSerialName
import kotlin.system.exitProcess

suspend fun createObjectSerializerName(name: String) = "${name.capitalize()}Serializer"

suspend fun createObjectSerializer(
  name: String,
  schema: PropertyTypes.Containers.Object,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  packageName: String
) = try {
  val refProps = when (schema.properties) {
    is Option.Some -> (schema.properties as Option.Some<List<PropertyTypes.Property.ObjectProperties>>).item.map {
      """
            "${it.name.createSerialName()}"
      """.trimIndent()
    }
    is Option.None -> listOf()
  }.joinToString(", ")
  val importList = mutableListOf<Pair<String, List<String>>>()
  val regexes = mutableListOf<String>()
  val comprehendPatterns = Option.comprehension<Triple<Boolean, Boolean, String>> {
    val (patterns) = schema.pattern
    val numPatterns = patterns.keys.size
    val (typeMap, regexImportList) = createPatternMapTypeMap(name, patterns, config)
    regexes.addAll(regexImportList.second)
    importList.add(regexImportList)
    Triple(numPatterns == 1, numPatterns > 1, typeMap)
  }
  val (patternProperty, patternProperties, regexTypeMap) = when (comprehendPatterns) {
    is Option.Some -> comprehendPatterns.item
    is Option.None -> Triple(first = false, second = false, third = "mapOf()")
  }
  val (className, _, _) = mutateNameIfEnabled(name, config)
  val serializerType = TypeSpec.objectBuilder(createObjectSerializerName(className))
    .superclass(
      AJSONSchemaSerializer::class.toClassName()
        .parameterizedBy(
          ClassName(packageName, className)
        )
    )
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .addSuperclassConstructorParameter("%L", "$className.serializer()")
    .addProperty(
      PropertySpec.builder("additionalProperties", Boolean::class)
        .addModifiers(KModifier.OVERRIDE)
        .initializer("%L", schema.additionalProperties.isSome())
        .build()
    )
    .addProperty(
      PropertySpec.builder("patternProperties", Boolean::class)
        .addModifiers(KModifier.OVERRIDE)
        .initializer("%L", patternProperties)
        .build()
    )
    .addProperty(
      PropertySpec.builder("patternProperty", Boolean::class)
        .addModifiers(KModifier.OVERRIDE)
        .initializer("%L", patternProperty)
        .build()
    )
    .addProperty(
      PropertySpec.builder("regexes", Set::class.parameterizedBy(Regex::class))
        .addModifiers(KModifier.OVERRIDE)
        .initializer(
          "%L",
          if (schema.pattern.isSome()) {
            "setOf(${regexes.joinToString(", ") { it }})"
          } else "setOf()"
        )
        .build()
    )
    .addProperty(
      PropertySpec.builder("regexToType", Map::class.parameterizedBy(Regex::class, String::class))
        .addModifiers(KModifier.OVERRIDE)
        .initializer("%L", regexTypeMap)
        .build()
    )
    .addProperty(
      PropertySpec.builder(
        "properties",
        ClassName("kotlin.collections", "List").parameterizedBy(ClassName("kotlin", "String"))
      )
        .addModifiers(KModifier.OVERRIDE)
        .initializer("%L", "listOf($refProps)")
        .build()
    )
    .build()
  Pair(serializerType, importList)
} catch (e: Exception) {
  logger.error(e) { "Encountered an unexpected exception while creating object: $name, error of: ${e.message}" }
  e.printStackTrace()
  exitProcess(1)
}
