/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import org.gradle.api.Plugin
import org.gradle.api.Project

@ExperimentalSerializationApi
class JsonSchemaGenerator : Plugin<Project> {
  override fun apply(project: Project) {
    val extension = project.extensions.create<design.animus.kotlin.mp.schemas.json.generator.JsonSchemaGeneratorConfig>(
      "JsonSchemaGeneratorConfig",
      design.animus.kotlin.mp.schemas.json.generator.JsonSchemaGeneratorConfig::class.java
    )
    project.task("jsonSchemaGenerate")
      .apply {
        group = "json_schema"
      }.doFirst { _ ->
        runBlocking {
          extension.schema.forEach { config ->
            logger.debug { "Processing schema for ${config.schemaFile} ${config.schemaFile.exists()}" }
            JSONSchemaGeneratorExecutor.generate(config.schemaFile.absolutePath, config)
          }
        }
      }
  }
}
