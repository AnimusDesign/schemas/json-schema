/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import design.animus.kotlin.contract.Versions.Dependencies

kotlin {
  jvm {
    compilations.all {
      kotlinOptions.jvmTarget = "11"
      kotlinOptions.apiVersion = "1.4"
    }
  }
  js(BOTH) {
    browser()
    nodejs()
    compilations.named("main") {
      kotlinOptions {
        metaInfo = true
        sourceMap = true
        verbose = true
        moduleKind = "umd"
      }
    }
  }
  val hostOs = System.getProperty("os.name")
  val isMingwX64 = hostOs.startsWith("Windows")
  val nativeTarget = when {
    hostOs == "Mac OS X" -> {
      iosX64 {}
      iosArm64 {}
      iosArm32 {}
      tvos {}
      tvosArm64 {}
      macosX64 {}
    }
    hostOs == "Linux" -> linuxX64 {}
    else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
  }
  sourceSets {
    val commonMain by getting {
      dependencies {
        api(project(":json_core:"))
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        implementation("io.github.microutils:kotlin-logging:${Dependencies.kotlinLogging}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-json:${Dependencies.serialization}")
      }
    }
    val commonTest by getting {
      dependencies {
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
      }
    }
    val jvmMain by getting {
      dependencies {
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
      }
    }
    val jvmTest by getting {
      dependencies {
        implementation(kotlin("test"))
        implementation(kotlin("test-junit"))
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
      }
    }
    val jsMain by getting {
      dependencies {
        implementation("io.github.microutils:kotlin-logging-js:${Dependencies.kotlinLogging}")
      }
    }
    val jsTest by getting {
      dependencies {
        implementation(kotlin("test-js"))
      }
    }
    val nativeMain by creating {
      dependsOn(commonMain)
    }
    val nativeTest by creating {
      dependsOn(commonMain)
    }
    when (nativeTarget.name) {
      "macosX64" -> {
        val iOSBase by creating {
          dependsOn(nativeMain)
        }
        val iosX64Main by getting {
          dependsOn(iOSBase)
        }
        val iosArm64Main by getting {
          dependsOn(iOSBase)
        }
        val iosArm32Main by getting {
          dependsOn(iOSBase)
        }
        val tvosMain by getting {
          dependsOn(iOSBase)
        }
        val tvosArm64Main by getting {
          dependsOn(iOSBase)
        }
        val macosX64Main by getting {
          dependsOn(nativeMain)
        }
      }
      "mingwX64" -> {
        val mingwX64Main by getting {
          dependsOn(nativeMain)
        }
        val mingwX64Test by getting {
          dependsOn(nativeTest)
        }
      }
      "linuxX64" -> {
        val linuxX64Main by getting {
          dependsOn(nativeMain)
        }
        val linuxX64Test by getting {
          dependsOn(nativeTest)
        }
      }
    }
  }
}

tasks {
  val jvmTest by getting(org.jetbrains.kotlin.gradle.targets.jvm.tasks.KotlinJvmTest::class) {
    this.useJUnit()
    this.reports.junitXml.isEnabled = true
  }
}
