/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaAnyContainer
import design.animus.kotlin.mp.schemas.json.core.serializers.AAnyContainerSerializer
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.JsonElement

@ExperimentalSerializationApi
suspend fun createAnyContainer(
  name: String,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
): TypeSpec {
  val serializerName = createObjectSerializerName(name)
  val anyObj = TypeSpec.classBuilder(name)
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .addAnnotation(
      AnnotationSpec.builder(kotlinx.serialization.Serializable::class)
        .addMember("with=%L::class", serializerName)
        .build()
    )
    .addSuperinterface(
      IJSONSchemaAnyContainer::class.toClassName()
    )
    .primaryConstructor(
      FunSpec.constructorBuilder()
        .addParameter("items", JsonElement::class)
        .build()
    )
    .addProperty(
      PropertySpec.builder("items", JsonElement::class)
        .addModifiers(KModifier.OVERRIDE)
        .initializer("items")
        .build()
    )
    .build()
  return anyObj
}

@ExperimentalSerializationApi
suspend fun createAnySerializer(
  name: String,
  packageName: String,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
): TypeSpec {
  val serializerName = createObjectSerializerName(name)
  val serializer = TypeSpec.objectBuilder(serializerName)
    .superclass(
      AAnyContainerSerializer::class.toClassName()
        .parameterizedBy(
          ClassName(packageName, name)
        )
    )
    .addSuperclassConstructorParameter("%L", "$name.serializer()")
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .build()
  return serializer
}
