package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.mp.schemas.json.core.interfaces.IUnionBase
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.TypeResponse
import design.animus.kotlin.mp.schemas.json.generator.logger
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.serialization.KSerializer

internal suspend fun buildNestedClass(
  subClassName: String,
  parentClassRef: ClassName,
  typeResponse: TypeResponse,
  wrapper: UnionWrapper = UnionWrapper.None,
  wrapperPosition: UnionWrapperPosition = UnionWrapperPosition.External,
  config: GeneratorConfig
): Pair<TypeSpec, PropertySpec> = try {
  logger.info { "Building nested class: $subClassName, with a parent ref of ${parentClassRef.packageName}/${parentClassRef.simpleName}" }
  logger.info { "Wrapper is set to $wrapper and position to $wrapperPosition" }
  val baseObj = TypeSpec.classBuilder(subClassName)
    .superclass(parentClassRef)
    .addModifiers(KModifier.DATA)
  when (typeResponse) {
    TypeResponse.Empty, is TypeResponse.Name<*>, is TypeResponse.Parameterized -> {
      println("===============")
      println("In build nested class")
      println(typeResponse)
      logger.info { "Type response is of empty, name, or parameterized." }
      val refType = createWrappedType(typeResponse, wrapper, wrapperPosition)
      println(refType)
      println(refType.annotations)
      println(refType.tags)
      val serializerType = when (refType) {
        is ClassName -> refType.copy(annotations = listOf())
        is ParameterizedTypeName -> {
          val nestedType = refType.typeArguments.map {
            it.copy(annotations = listOf())
          }
          (refType.rawType.copy(annotations = listOf()) as ClassName).parameterizedBy(*nestedType.toTypedArray())
        }
        else -> error("Type Name of: $refType should not be cast here.")
      }
      val serializerName = when (refType) {
        is ClassName -> {
          println("Ref type is class name")
          val base = refType.copy(annotations = listOf()) as ClassName
          if (refType.annotations.isNotEmpty()) {
            refType.annotations.first().members.first()
              .toString()
              .replace("with =", "")
              .replace("with=", "")
              .replace("::class", "")
          } else if ((typeResponse is TypeResponse.Name<*>) && typeResponse.definition.isSome() && typeResponse.definition.forceSome().type is PropertyTypes.Containers.Array) {
            "${base}Serializer"
          } else {
            "$base.serializer()"
          }
//          else if (refType.packageName.contains(config.packageBase)) {
//            "${base}Serializer"
//          }
        }
        is ParameterizedTypeName -> {
          println("Ref type is ParameterizedTypeName")
          val nestedType = refType.typeArguments.map {
            it.copy(annotations = listOf())
          }.firstOrNull() as? ClassName ?: error("Expecting only a single nested type")
          val nestedSerializer = if (nestedType.packageName.contains(config.packageBase)) {
            "${nestedType.packageName}.${nestedType.simpleName}Serializer"
          } else {
            "${nestedType.simpleName}.serializer()"
          }
          val base = "${(refType.rawType.copy(annotations = listOf()) as ClassName).simpleName}Serializer"
          "$base($nestedSerializer)"
        }
        else -> error("Type Name of: $refType should not be cast here.")
      }
      logger.info { "Determined type is $refType" }
      Pair(
        baseObj
          .addSuperinterface(IUnionBase::class.toClassName().parameterizedBy(refType))
          .primaryConstructor(
            FunSpec.constructorBuilder()
              .addParameter("item", refType)
              .build()
          )
          .addProperty(
            PropertySpec.builder("item", refType)
              .addModifiers(KModifier.OVERRIDE)
              .initializer("item")
              .build()
          )
          .build(),
        PropertySpec.Companion.builder(
          "${subClassName.decapitalize()}Serializer",
          KSerializer::class.toClassName().parameterizedBy(serializerType)
        )
          .addModifiers(KModifier.PRIVATE)
          .initializer(serializerName)
          .build()
      )
    }
  }
} catch (e: Exception) {
  logger.error { "Encountered unexpected exception in union base/buildNestedClass: ${e.message}" }
  throw e
}
