# JSON Schema

A kotlin multi platform model generator from JSON Schema.

* [What is JSON Schema?](https://json-schema.org/).
    * It is a way to describe messages and data passing between services, or being provided by services. 
        It is also used to validate configuration data.
* What is this?
    * This will read the JSON schema document and generate   


* **License**: Apache


# Features

| Feature                                          | Supported | Planned | Notes                                                                                                               |
|--------------------------------------------------|-----------|---------|---------------------------------------------------------------------------------------------------------------------|
| Draft Support                                    | ✔         |         |                                                                                                                     |
| Draft v4                                         | ✔         |         |                                                                                                                     |
| Draft v6                                         | ❌         | ✔       |                                                                                                                     |
| Draft v7                                         | ✔         |         |                                                                                                                     |
| Reference Local                                  | ✔         |         |                                                                                                                     |
| Reference Relative                               | ⎺         | Yes     |                                                                                                                     |
| Reference External                               | ✔         |         |                                                                                                                     |
| oneOf, anyOf, allOf                              | ◐         | Yes     | Generated as sealed class, but lacking validation.                                                                  |
| patternProperties                                | ◐         | Yes     | Generation needs a special helper method, intend to extend HashMap, or a custom object in the future.               |
| Gradle Plugin                                    | ✔         |         |                                                                                                                     |
| CLI                                              | ⎺         | Yes     | A Java CLI will be provided. The code generator requires JVM.                                                       |
| Multi Platform                                   | ⎺         | Yes     | Native is planned, inherited libraries need to be updated. JS is output, but `@JSExport` doesn't work at this time. |
| JSON Schema type & format                        | ✔         |         |                                                                                                                     |
| Description / Comments and general documentation | ❌         | Yes     | KDoc via dokka will be used in the future.                                                                          |
| Required vs Optional Fields                      | ✔         |         | Non required fields are nullable and default to null.                                                               |
| Enum                                             | ✔         |         |                                                                                                                     |


# Motivation

[See this post](https://musings.animus.design/language-of-the-cloud/) where I go over the language of the cloud. JSON 
schema backs two critical components of cloud native development. Inter service communication, outside of proto buf or avro.
Secondarily deployment and infrastructure.

By providing a platform agnostic tool to generate code off the schemas. It allows development of these models in a type safe
module way.

**Sponsor / Owner**

<img src="https://animus.design/images/logo-doubleshot-software.png"   height="120">

<img src="https://media-exp1.licdn.com/dms/image/C4E0BAQFMZJmwtTKpwg/company-logo_200_200/0?e=1614211200&v=beta&t=03S5IBzGa1HOodBCEE9wjo3OK0ONYfkg_HBgQSYsxm0" height="120" />
