package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.utils.getSchemaInterfaceClassName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

suspend fun createSchemaInterface(config: GeneratorConfig) {
  val (className, _, _) = config.schemaName.sanitizeForClassName()
  val finalName = "I$className"
  val file = fileSpecBootStrap(config.packageBase, finalName)
  val type = TypeSpec.interfaceBuilder(finalName)
    .addSuperinterface(IJSONSchemaObjectBase::class.toClassName())
    .build()
  file.addType(type).build().writeTo(config.outputPath)
  createSchemaInterfaceSerializer(config)
}

/**
 * todo Switch from try/catch to while until not null
 */
suspend fun FunSpec.Builder.addDeserializeAttempt(
  propName: String,
  serializerName: String,
  packageName: String
): FunSpec.Builder {
  this.addStatement(
    """val %L = try {
                    decoder.decodeSerializableValue(%T)
            } catch(e:Exception) {
                null
            }
    """.trimIndent(),
    propName,
    ClassName(packageName, serializerName)
  )
  return this
}

suspend fun createPropName(packageName: String, serializerName: String) =
  "possible${packageName.split(".").joinToString("") { it.capitalize() }}${serializerName.capitalize()}"

suspend fun createSchemaInterfaceSerializer(config: GeneratorConfig) {
  val (className, _, _) = config.schemaName.sanitizeForClassName()
  val finalName = createObjectSerializerName("I$className")
  val schemaInterface = config.getSchemaInterfaceClassName()
  val file = fileSpecBootStrap(config.packageBase, finalName)
  val encoderFunc = FunSpec.builder("serialize")
    .addModifiers(KModifier.OVERRIDE)
    .addParameter("encoder", Encoder::class.toClassName())
    .addParameter("value", schemaInterface)
    .addCode("return encoder.encodeSerializableValue(value.getAJSONSchemaSerializer(), value)")
    .build()

  val decoderFunc = FunSpec.builder("deserialize")
    .addParameter("decoder", Decoder::class.toClassName())
    .addModifiers(KModifier.OVERRIDE)
    .returns(schemaInterface)
  val serializers = jsonSchema.definitions.mapNotNull { def ->
    if (def.type is PropertyTypes.Containers.Object && !(def.type as PropertyTypes.Containers.Object).isEmpty()) {
      val name = def.propertyName!!
      val packageName = mutatePackageNameIfEnabled("${config.packageBase}.definitions", name, config)
      val (defName, _, _) = mutateNameIfEnabled(name, config)
      val serializerName = createObjectSerializerName(defName)
      ClassName(packageName, serializerName)
    } else {
      null
    }
  }.toMutableList()
  if (config.createBaseObject) {
    val parentSerializer = createObjectSerializerName(config.schemaName).capitalize()
    val packageName = mutatePackageNameIfEnabled(config.packageBase, config.schemaName, config)
    serializers.add(ClassName(packageName, parentSerializer))
  }
  if (serializers.size == 1) {
    decoderFunc.addStatement(
      "return decoder.decodeSerializableValue(%T)",
      serializers.first()
    )
  } else {
    decoderFunc.addStatement(
      """
                val serializers = listOf(
                   ${serializers.joinToString(", ") { "${it.packageName}.${it.simpleName}" }}
                )
                var rsp : %T = null
                loop@ for (ser in serializers) {
                    if (rsp != null) break@loop
                    val attempt = try {
                        decoder.decodeSerializableValue(ser)
                    } catch(e:Exception) {
                        null
                    } as %T
                    rsp = attempt
                }
                return rsp ?: error("Could not find serializer")
      """.trimIndent(),
      schemaInterface.copy(nullable = true),
      schemaInterface.copy(nullable = true)
    )
  }
  val type = TypeSpec.objectBuilder(finalName)
    .addSuperinterface(KSerializer::class.toClassName().parameterizedBy(schemaInterface))
    .addAnnotation(experimentalSerializationClass)
    .addAnnotation(
      AnnotationSpec.builder(ClassName("kotlinx.serialization", "Serializer"))
        .addMember("forClass=%L::class", schemaInterface)
        .build()
    )
    .addFunction(encoderFunc)
    .addFunction(decoderFunc.build())
    .addProperty(
      PropertySpec.builder("descriptor", SerialDescriptor::class.toClassName())
        .addModifiers(KModifier.OVERRIDE)
        .initializer(
          """%T("%L", %T)""",
          ClassName("kotlinx.serialization.descriptors", "PrimitiveSerialDescriptor"),
          finalName,
          PrimitiveKind.STRING::class
        )
        .build()
    )
  file.addType(type.build()).build().writeTo(config.outputPath)
}
