package design.animus.kotlin.mp.schemas.json.core.interfaces

import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
interface IUnionBase<R> {
  val item: R
}
