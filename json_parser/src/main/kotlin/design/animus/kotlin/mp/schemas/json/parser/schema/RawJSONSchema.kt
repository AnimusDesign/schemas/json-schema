/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("PLUGIN_WARNING")

package design.animus.kotlin.mp.schemas.json.parser.schema

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.AdditionalProperties
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Required
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Type
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class JSONSchemaType {
  OBJECT,
  ARRAY
}

@Serializable
data class JSONSchemaPatternProperty(val x: String)

data class JSONSchemaProperty(
  val propertyName: String?,
  val type: PropertyTypes
) {
  suspend fun toObjectProperty() = PropertyTypes.Property.ObjectProperties(
    propertyName!!,
    type
  )
}

@Serializable
data class RawJSONSchema(
  @SerialName("\$schema")
  val schema: String,
  val type: Type? = Type.SingleType(SchemaTypes.OBJECT.friendly),
  @SerialName("\$id")
  val id: String = "",
  val title: String = "",
  val additionalProperties: AdditionalProperties? = null,
  val patternProperties: Map<String, RawJSONSchemaProperty> = mapOf(),
  val description: String = "",
  val properties: Map<String, RawProperty> = mapOf(),
  val items: RawJSONSchemaProperty? = null,
  val required: Required = Required.Empty,
  val dependencies: Map<String, List<String>> = mapOf(),
  val definitions: Map<String, RawJSONSchemaProperty> = mapOf()
)

sealed class JSONSchema

data class JSONSchemaObject(
  val id: String,
  val schema: String,
  val type: SchemaTypes,
  val title: String = "",
  val description: String = "",
  val properties: List<JSONSchemaProperty> = listOf(),
  val additionalProperties: Option<PropertyTypes> = Option.None(),
  val patternProperties: Map<Regex, PropertyTypes> = mapOf(),
  val required: PropertyTypes.Property.Required = PropertyTypes.Property.Required.ListOfStrings(listOf()),
  val dependencies: Map<String, List<String>> = mapOf(),
  val definitions: List<JSONSchemaProperty> = listOf()
) : JSONSchema() {
  suspend fun merge(subOj: JSONSchemaObject): JSONSchemaObject {
    val copy = this.copy()
    val mutableDefs = copy.definitions.toMutableList()
    mutableDefs.addAll(subOj.definitions)
    return copy.copy(definitions = mutableDefs)
  }
}

data class JSONSchemaArray(
  val items: JSONSchema
) : JSONSchema()
