/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.records.raw

import design.animus.kotlin.mp.schemas.json.parser.schema.unions.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RawJSONSchemaProperty(
  val type: Type? = null,
  val examples: List<String> = listOf(),
  @SerialName("\$ref")
  val ref: String? = null,
  val description: String? = "",
  val format: String? = null,
  val items: ArrayItems? = null,
  val properties: Map<String, RawJSONSchemaProperty>? = null,
  val enum: List<String>? = null,
  val required: Required = Required.Empty,
  val patternProperties: Map<String, RawJSONSchemaProperty> = mapOf(),
  val additionalProperties: AdditionalProperties? = null,
  val additionalItems: AdditionalItems? = null,
  val oneOf: List<RawJSONSchemaProperty> = listOf(),
  val anyOf: List<RawJSONSchemaProperty> = listOf(),
  val allOf: List<RawJSONSchemaProperty> = listOf(),
  val minItems: Int? = null,
  val maxItems: Int? = null,
  val minProperties: Int? = null,
  val maxProperties: Int? = null,
  val uniqueItems: Boolean = false,
  val propertyNames: RawJSONSchemaProperty? = null,
  val minLength: Int? = null,
  val default: Default = Default.Empty,
  val pattern: String? = null,
  @SerialName("const")
  val constant: String? = null,
  val not: RawJSONSchemaProperty? = null,
  val exclusiveMinimum: Int? = null,
  val minimum: PrimitiveIntOrProperty? = null,
  val exclusiveMaximum: Int? = null,
  val maximum: PrimitiveIntOrProperty? = null
)
