/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.contract.tests.gitlab

import design.animus.kotlin.mp.contract.simple.address.Address
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import org.junit.Test
import kotlin.test.assertTrue

@ExperimentalSerializationApi
class TestAddressObject {

  @Test
  fun testSerialize() {
    val address = Address(
      locality = "en-US",
      region = "US",
      countryName = "US"
    )
    val generated = Json.encodeToString(Address.serializer(), address)
    println(generated)
    val expected =
      """{"locality":"en-US","region":"US","country-name":"US"}"""
    println(expected)
    assertTrue("Address object serialized matches expected") {
      generated == expected
    }
  }

  @Test
  fun testDeserialize() {
    val raw =
      """
            {"post-office-box":null,"extended-address":null,"street-address":null,"locality":"en-US","region":"US","postal-code":null,"country-name":"US"}
      """.trimIndent()
    val parsed = Json.decodeFromString(Address.serializer(), raw)
    println(parsed)
    val expected = Address(
      locality = "en-US",
      region = "US",
      countryName = "US"
    )
    println(expected)
    assertTrue("Parsed JSON string matches expected") { parsed == expected }
  }
}
