/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.TypeAliasSpec
import com.squareup.kotlinpoet.TypeName
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.containers.*
import design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions.*
import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import java.io.File
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import mu.KotlinLogging

internal val logger = KotlinLogging.logger { }
internal lateinit var jsonSchema: JSONSchemaObject
internal val externalReferences = mutableMapOf<String, TypeName>()

@ExperimentalSerializationApi
object JSONSchemaGeneratorExecutor {
  suspend fun generate(path: String, config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig) {
    val content = File(path).readText()
    initiate(content, config)
  }

  suspend fun initiate(content: String, config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig) {
    when (config.mutateSchema.isSome()) {
      false -> content
      true -> {
        val mutateSchemaFunction = config.mutateSchema.forceSome()
        val asJson = Json.decodeFromString(JsonElement.serializer(), content)
        Json.encodeToString(JsonElement.serializer(), mutateSchemaFunction(asJson))
      }
    }
    when (val unknownSchema = JSONSchemaParser.parseJSON(content)) {
      is JSONSchemaObject -> processJSONSchemaObject(unknownSchema, config, config.createBaseObject)
    }
  }

  @ExperimentalSerializationApi
  suspend fun processJSONSchemaObject(
    schema: JSONSchemaObject,
    config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
    createBaseObject: Boolean = true
  ) {
    logger.info { "Processing definitions." }
    jsonSchema = if (::jsonSchema.isInitialized) {
      jsonSchema.merge(schema)
    } else {
      schema
    }
    createSchemaInterface(config)
    schema.definitions.forEach {
      logger.debug { "Processing definition: ${it.propertyName ?: "ERROR Unknown Name"}" }
      logger.debug { "Full configuration for definition is: $it" }
      val (definitionName, _, _) = mutateNameIfEnabled(it.propertyName!!, config)
      val packageName = mutatePackageNameIfEnabled(
        "${config.packageBase}.definitions",
        it.propertyName!!,
        config
      )
      logger.debug { "Final packageName:$packageName className:$definitionName, writing to ${config.outputPath} " }
      val file = fileSpecBootStrap(packageName, definitionName)
      when (it.type) {
        is PropertyTypes.Containers.Object -> {
          logger.info { "Creating Object ${it.propertyName}" }
          val objType = createObject(
            it.propertyName!!,
            it.type as PropertyTypes.Containers.Object,
            config
          )
          when (objType) {
            is CreateObjectResponse.Object -> {
              val (objSerializer, importList) = createObjectSerializer(
                definitionName,
                it.type as PropertyTypes.Containers.Object,
                config,
                packageName
              )
              importList.forEach { (packageName, classNames) ->
                file.addImport(packageName, *classNames.toTypedArray())
              }
              file
                .addType(objType.obj)
                .addType(objSerializer)
            }
            is CreateObjectResponse.Alias -> {
              logger.warn { "Object definition appears empty, created as a type alias." }
              file.addTypeAlias(objType.alias)
            }
          }
          logger.debug { "Writing file to package: $packageName class name: $definitionName" }
          file.build().writeTo(config.outputPath)
        }
        is PropertyTypes.Property.OneOf -> {
          logger.info { "Creating Union One Of ${it.propertyName}" }
          createOneOf(
            it.propertyName!!,
            it.type as PropertyTypes.Property.OneOf,
            config,
            UnionWrapper.None,
            UnionWrapperPosition.Internal,
            Option.Some("${config.packageBase}.definitions")
          )
        }
        is PropertyTypes.Property.AllOf -> {
          logger.info { "Creating Union One Of ${it.propertyName}" }
          createAllOf(
            it.propertyName!!,
            it.type as PropertyTypes.Property.AllOf,
            config,
            UnionWrapper.None,
            UnionWrapperPosition.Internal,
            Option.Some("${config.packageBase}.definitions")
          )
        }
        is PropertyTypes.Property.AnyOf -> {
          logger.info { "Creating Union One Of ${it.propertyName}" }
          createAnyOf(
            it.propertyName!!,
            it.type as PropertyTypes.Property.AnyOf,
            config,
            UnionWrapper.None,
            UnionWrapperPosition.Internal,
            Option.Some("${config.packageBase}.definitions")
          )
        }
        is PropertyTypes.Property.AnonymousObject -> {
          val anonymousName = "Anonymous${it.propertyName!!.capitalize()}"
          val packageName =
            mutatePackageNameIfEnabled("${config.packageBase}.anonymous", it.propertyName!!, config)
          createInlineObject(
            anonymousName,
            (it.type as PropertyTypes.Property.AnonymousObject).toObjectContainer(),
            config
          )
          val anonymousAlias = TypeAliasSpec.builder(
            it.propertyName!!.capitalize(),
            ClassName(packageName, anonymousName)
          )
            .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
            .build()
          val anonymousSerializer = TypeAliasSpec.builder(
            createObjectSerializerName(it.propertyName!!),
            ClassName(packageName, createObjectSerializerName(anonymousName))
          )
            .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
            .build()
          file.addTypeAlias(anonymousAlias)
            .addTypeAlias(anonymousSerializer)
            .build().writeTo(config.outputPath)
        }
        is PropertyTypes.Containers.Array -> createArrayTypeAlias(
          it,
          it.type as PropertyTypes.Containers.Array,
          config
        )
        is PropertyTypes.Containers.EnumProperty -> createEnum(
          Option.None(),
          it.propertyName!!,
          it.type as PropertyTypes.Containers.EnumProperty,
          config,
          isDefinition = true
        )
        is PropertyTypes.Property.BasicProperty -> {
          logger.info { "Found a basic property creating as type alias" }
          val (aliasName, _, _) = mutateNameIfEnabled(it.propertyName!!, config)
          val packageName =
            mutatePackageNameIfEnabled("${config.packageBase}.definitions", it.propertyName!!, config)
          val typeAlias = propertyToClass(aliasName, it.type, config)
            .toTypeName()
          val spec = TypeAliasSpec.builder(aliasName, typeAlias)
            .build()
          val file = fileSpecBootStrap(packageName, aliasName)
          file.addTypeAlias(spec)
          file.build().writeTo(config.outputPath)
        }
        is PropertyTypes.Property.AnyProperty -> {
          logger.debug { "Found an any property casting as a wild card object." }
          val anyContainer = createAnyContainer(definitionName, config)
          val anySerializer = createAnySerializer(definitionName, packageName, config)
          file.addType(anyContainer)
            .addType(anySerializer)
          file.build().writeTo(config.outputPath)
        }
        else -> {
          logger.warn { "Definition: ${it.propertyName} of type: ${it.type} is not a container or union type cannot create a definition" }
          logger.warn { it }
          null
        }
      }
    }
    if (createBaseObject) {
      val file = fileSpecBootStrap(config.packageBase, config.schemaName)
      val objContainer = PropertyTypes.Containers.Object(
        properties = when (schema.properties.isNotEmpty()) {
          true -> Option.Some(schema.properties.map { it.toObjectProperty() })
          false -> Option.None()
        },
        additionalProperties = schema.additionalProperties,
        pattern = if (schema.patternProperties.isEmpty()) Option.None() else Option.Some(schema.patternProperties),
        required = Option.Some(schema.required)

      )
      val baseObj = createObject(
        config.schemaName,
        objContainer,
        config
      )
      when (baseObj) {
        is CreateObjectResponse.Object -> {
          val (serializer, imports) = createObjectSerializer(
            config.schemaName,
            objContainer,
            config,
            config.packageBase
          )
          imports.forEach { (packageName, classNames) ->
            file.addImport(packageName, *classNames.toTypedArray())
          }
          file
            .addType(baseObj.obj)
            .addType(serializer)
        }
        is CreateObjectResponse.Alias -> {
          file.addTypeAlias(baseObj.alias)
        }
      }
      file
        .build()
        .writeTo(config.outputPath)
    }
  }
}
