/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJsonSchemaEnum
import design.animus.kotlin.mp.schemas.json.core.serializers.AEnumSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertTrue

@Serializable(with = SampleEnumSerializer::class)
enum class SampleEnum(override val friendly: String) : IJsonSchemaEnum<SampleEnum> {
  Of200("2.0.0"),
  Of101("1.0.1")
}

object SampleEnumSerializer : AEnumSerializer<SampleEnum>(SampleEnum::values)

@Serializable
data class TestEnum(
  val value: SampleEnum
)

class EnumTest {
  @Test
  fun testDeserializeOf200() {
    val raw200 =
      """{
            "value": "2.0.0"
        }""".trimMargin()
    val data200 = Json.decodeFromString(TestEnum.serializer(), raw200)
    assertTrue("Enum value is parsed properly") {
      data200.value == SampleEnum.Of200
    }
  }

  @Test
  fun testDeserializeOf101() {
    val raw101 =
      """{
            "value": "1.0.1"
        }""".trimMargin()
    val data101 = Json.decodeFromString(TestEnum.serializer(), raw101)
    assertTrue("Enum value is parsed properly") {
      data101.value == SampleEnum.Of101
    }
  }

  @Test
  fun testSerializeOf200() {
    val data = TestEnum(SampleEnum.Of200)
    val serialized = Json.encodeToString(TestEnum.serializer(), data)
    val expected = "{\"value\":\"2.0.0\"}"
    assertTrue { serialized == expected }
  }

  @Test
  fun testSerializeOf101() {
    val data = TestEnum(SampleEnum.Of101)
    val serialized = Json.encodeToString(TestEnum.serializer(), data)
    val expected = "{\"value\":\"1.0.1\"}"
    assertTrue { serialized == expected }
  }
}
