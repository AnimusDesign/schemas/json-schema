/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions.UnionWrapper
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlin.reflect.KClass
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JsonElement

suspend fun createArray(
  name: String,
  prop: PropertyTypes.Containers.Array,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
): TypeResponse {
  logger.debug { "Creating array for: $name" }
  logger.debug { "Schema is: $prop" }
  return if (prop.items is PropertyTypes.Property.OneOf || prop.items is PropertyTypes.Property.AllOf || prop.items is PropertyTypes.Property.AnyOf) {
    logger.debug { "Array items is of union type." }
    val subClass = propertyToClass(name, prop.items, config, UnionWrapper.None)
    TypeResponse.Parameterized(
      List::class.toClassName().parameterizedBy(subClass.toTypeName())
    )
  } else {
    when (val subClass = propertyToClass(name, prop.items, config, namePrefix = namePrefix)) {
      TypeResponse.Empty -> TypeResponse.Parameterized(
        ClassName("kotlin.collections", "List").parameterizedBy(
          JsonElement::class.toClassName()
        )
      )
      is TypeResponse.Name<*> -> TypeResponse.Parameterized(
        ClassName("kotlin.collections", "List").parameterizedBy(subClass.className)
      )
      is TypeResponse.Parameterized -> TypeResponse.Parameterized(
        ClassName("kotlin.collections", "List").parameterizedBy(subClass.parameterized)
      )
      else -> subClass
    }
  }
}

suspend fun getSerializerInitializer(
  type: TypeName,
  config: GeneratorConfig,
  wrapper: Option<out KClass<out Collection<*>>> = Option.None()
): String {
  val baseSerializer = when (type) {
    is ClassName -> {
      val base = type.copy(annotations = listOf()) as ClassName
      if (type.annotations.isNotEmpty()) {
        type.annotations.first().members.first()
          .toString()
          .replace("with =", "")
          .replace("with=", "")
          .replace("::class", "")
      } else {
        "$base.serializer()"
      }
    }
    is ParameterizedTypeName -> {
      val nestedType = type.typeArguments.map {
        it.copy(annotations = listOf())
      }.firstOrNull() as? ClassName ?: error("Expecting only a single nested type")
      val nestedSerializer = if (nestedType.packageName.contains(config.packageBase)) {
        "${nestedType.simpleName}Serializer"
      } else {
        "${nestedType.simpleName}.serializer()"
      }
      val base = "${(type.rawType.copy(annotations = listOf()) as ClassName).simpleName}Serializer"
      "$base($nestedSerializer)"
    }
    else -> error("Type Name of: $type should not be cast here.")
  }
  return when (wrapper) {
    is Option.Some -> {
      val wrapperClass = wrapper.item
      println(wrapperClass)
      when (wrapperClass) {
        List::class -> {
          "ListSerializer($baseSerializer)"
        }
        Set::class -> {
          "SetSerializer($baseSerializer)"
        }
        Map::class -> {
          "MapSerializer(String.serializer(), $baseSerializer)"
        }
        else -> {
          error("Unknown wrapper class ")
        }
      }
    }
    is Option.None<*> -> {
      baseSerializer
    }
  }
}

suspend fun getSerializerType(
  type: TypeName,
  config: GeneratorConfig,
  wrapper: Option<out KClass<out Collection<*>>> = Option.None()
): TypeName {
  return when (type) {
    is ClassName -> type.copy(annotations = listOf())
    is ParameterizedTypeName -> {
      val nestedType = type.typeArguments.map {
        it.copy(annotations = listOf())
      }
      (type.rawType.copy(annotations = listOf()) as ClassName).parameterizedBy(*nestedType.toTypedArray())
    }
    else -> error("Type Name of: $type should not be cast here.")
  }
}

suspend fun createArrayTypeAlias(
  prop: JSONSchemaProperty,
  arrayDef: PropertyTypes.Containers.Array,
  config: GeneratorConfig
) {
  val packageName = "${config.packageBase}.definitions"
  val (arrayName, _, _) = prop.propertyName!!.sanitizeForClassName()
  val itemType = propertyToClass(arrayName, arrayDef.items, config).toTypeName()
  val arrayType = (if (arrayDef.uniqueItems) Set::class else List::class)
    .toClassName()
    .parameterizedBy(itemType)
  val arrayTypeAlias = TypeAliasSpec.builder(arrayName.capitalize(), arrayType)
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
  if (arrayDef.description.isSome()) arrayTypeAlias.addKdoc(arrayDef.description.forceSome())
  val wrapperType = when (arrayDef.uniqueItems) {
    true -> Set::class.toClassName()
    false -> List::class.toClassName()
  }
  fileSpecBootStrap(packageName, arrayName)
    .addTypeAlias(arrayTypeAlias.build())
    .addImport("kotlinx.serialization.builtins", "ListSerializer")
    .addImport("kotlinx.serialization.builtins", "serializer")
    .addProperty(
      PropertySpec.builder(
        "${arrayName.capitalize()}Serializer",
        KSerializer::class.toClassName().parameterizedBy(wrapperType.parameterizedBy(getSerializerType(itemType, config)))
      )
        .initializer(
          getSerializerInitializer(
            itemType,
            config,
            wrapper = if (arrayDef.uniqueItems) Option.Some(Set::class) else Option.Some(List::class)
          )
        )
        .build()
    )
    .build()
    .writeTo(config.outputPath)
}
