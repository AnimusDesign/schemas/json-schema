package design.animus.kotlin.mp.schemas.json.diff

enum class Actions {
  Delete,
  Update,
  Create;
}
