/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core.serializers

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import design.animus.kotlin.mp.schemas.json.core.interfaces.additionalPropertiesKey
import design.animus.kotlin.mp.schemas.json.core.interfaces.schemaPatternPropertiesKey
import design.animus.kotlin.mp.schemas.json.core.logger
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.*
import kotlin.collections.component1
import kotlin.collections.component2

abstract class AJSONSchemaSerializer<O : IJSONSchemaObjectBase>(private val serializer: KSerializer<O>) :
  KSerializer<O> {
  abstract val properties: List<String>
  abstract val additionalProperties: Boolean
  abstract val patternProperty: Boolean
  abstract val patternProperties: Boolean
  abstract val regexes: Set<Regex>
  abstract val regexToType: Map<Regex, String>

  override val descriptor = buildClassSerialDescriptor("AJSONSchemaObjectBaseSerializer")

  private fun makeMapNonNull(map: Map<String, JsonElement?>): Map<String, JsonElement> {
    return map.filter { (_, value) -> value != null }.map { (k, v) -> k to v!! }.toMap()
  }

  override fun deserialize(decoder: Decoder): O {
    logger.debug { "In Decoder for ${this::class.simpleName}" }
    val input = decoder as? JsonDecoder ?: throw SerializationException("Expected Json Input")
    val rawObj = input.decodeJsonElement() as? JsonObject ?: throw SerializationException("Expected JsonObject")
    logger.debug { "Cast to raw object: $rawObj" }
    val map = rawObj.keys.filter { properties.contains(it) }.map {
      it to rawObj[it]
    }
      .filter { (_, v) -> v != null }
      .toMap().toMutableMap()
    logger.debug { "Cast to mutable map: $map" }
    val patternProps = if (patternProperty || patternProperties) {
      rawObj.keys.filter { k ->
        regexes.any { r ->
          r.containsMatchIn(k)
        }
      }
    } else {
      listOf()
    }
    logger.debug { "Found pattern props of: $patternProps" }
    if (patternProps.isNotEmpty() && patternProperty) {
      logger.debug { "Target is instance of pattern property" }
      val patternProperties = mapOf(
        "map" to JsonObject(
          makeMapNonNull(
            patternProps.map { k -> k to rawObj[k] }.toMap()
          )
        )
      )
      logger.debug { "Generated pattern properties of: $patternProperties" }
      map[schemaPatternPropertiesKey] = if (patternProperties["map"]?.keys?.isEmpty() != false) {
        logger.debug { "Keys are empty returning null" }
        null
      } else {
        logger.debug { "Making map non null" }
        logger.debug { makeMapNonNull(patternProperties) }
        JsonObject(makeMapNonNull(patternProperties))
      }
    }
    if (patternProps.isNotEmpty() && patternProperties) {
      logger.debug { "Target is instance of pattern properties" }
      val patternPropertiesByRegex = patternProps.groupBy { k ->
        regexes.first { it.containsMatchIn(k) }
      }
      logger.debug { "Grouped pattern properties by regex of: $patternPropertiesByRegex" }
      val patternProperties = patternPropertiesByRegex.map { (regex, props) ->
        val regexTypeKey = regexToType.keys.first { it.pattern == regex.pattern }
        JsonObject(
          mapOf(
            "type" to JsonPrimitive(regexToType[regexTypeKey]),
            "regex" to JsonPrimitive(regex.pattern),
            "map" to JsonObject(
              makeMapNonNull(
                props.map { key -> key to rawObj[key] }.toMap()
              )
            )
          )
        )
      }
      logger.debug { "Built pattern properties of: $patternProperties" }
      map[schemaPatternPropertiesKey] = if (patternProperties.isEmpty()) {
        null
      } else {
        JsonArray(patternProperties)
      }
    }
    if (additionalProperties) {
      logger.debug { "Target is instance of additional properties." }
      val additionalProps = rawObj.keys.filter {
        !properties.contains(it) && !patternProps.contains(it)
      }.map {
        it to rawObj[it]
      }.toMap()
      logger.debug { "Item has the following additional properties: $additionalProps" }
      map[additionalPropertiesKey] = if (additionalProps.keys.isEmpty()) {
        null
      } else {
        JsonObject(makeMapNonNull(additionalProps))
      }
    }
    logger.debug { "Properties is empty: ${properties.isEmpty()}" }
    logger.debug { "Additional properties is empty: ${map[additionalPropertiesKey] == null}" }
    logger.debug { "Pattern Properties is empty ${map[schemaPatternPropertiesKey] == null}" }
    if (map.isNullOrEmpty()) {
      throw SerializationException("All values in map are null.")
    }
    if (map[additionalPropertiesKey] == null && additionalProperties) {
      map[additionalPropertiesKey] = JsonObject(mapOf())
    }
    if (map[schemaPatternPropertiesKey] == null && (patternProperties || patternProperty)) {
      map[schemaPatternPropertiesKey] = if (patternProperties) JsonArray(listOf()) else null
    }
    val finalObj = JsonObject(
      makeMapNonNull(map)
    )
    logger.debug { "Built a final object of: $finalObj" }
    val rsp = Json.decodeFromJsonElement(serializer, finalObj)
    logger.debug { "Response is: $rsp" }
    return rsp
  }

  override fun serialize(encoder: Encoder, value: O) {
    logger.debug { "Attempting to encode value: $value of ${value::class.simpleName}" }
    val raw = Json.encodeToJsonElement(serializer, value) as?
      JsonObject ?: throw SerializationException("Expected Object")
    val map = raw.keys.map {
      it to raw[it]
    }.toMap().toMutableMap()
    logger.debug { "Cast to mutable map: $map" }

    val mergePropertiesUp: (String) -> Unit = { key ->
      if (map.containsKey(key)) {
        logger.debug { "Found $key in object" }
        val additionalProperties = map[key] as?
          JsonObject ?: throw SerializationException("Expected JsonObject")
        additionalProperties.keys.forEach { k ->
          map[k] = additionalProperties[k]
        }
        map.remove(key)
      }
    }
    if (map.containsKey(schemaPatternPropertiesKey)) {
      logger.debug { "Found patternProperties in object" }
      if (patternProperty) {
        val patternProps = map[schemaPatternPropertiesKey] as? JsonObject
          ?: throw SerializationException("Expected patternProperty to be an object")
        val data = patternProps["map"] as? JsonObject
          ?: throw SerializationException("Could not cast patternProperties.map to JsonObject")
        data.filter { (k, v) -> regexes.any { it.matches(k) } }.forEach { (k, v) ->
          map[k] = v
        }
      }
      if (patternProperties) {
        val patternSet = map[schemaPatternPropertiesKey] as? JsonArray
          ?: throw SerializationException("Expected patternProperties to be an array")
        patternSet.forEach {
          val obj = it as JsonObject
          val data = obj["map"] as? JsonObject
            ?: throw SerializationException("Could not cast pattern property map to json object")
          data
            .filter { (k, v) -> regexes.any { r -> r.matches(k) } }
            .map { (k, v) ->
              map[k] = v
            }
        }
      }
      map.remove(schemaPatternPropertiesKey)
    }
    mergePropertiesUp(additionalPropertiesKey)

    val finalObj = JsonObject(
      makeMapNonNull(map)
    )
    encoder.encodeSerializableValue(JsonObject.serializer(), finalObj)
  }
}
