/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core.serializers

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJsonSchemaEnum
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Suppress("UNCHECKED_CAST")
abstract class AEnumSerializer<S : Enum<S>>(private val values: () -> Array<S>) : KSerializer<S> {
  override val descriptor: SerialDescriptor =
    PrimitiveSerialDescriptor("AEnumSerializer", PrimitiveKind.STRING)
  var jsonSchemaValues: List<IJsonSchemaEnum<S>> = values().map { value ->
    value as? IJsonSchemaEnum<S> ?: error("Failed to cast value as a JSON Schema Enum")
  }

  override fun deserialize(decoder: Decoder): S {
    val passedValue = decoder.decodeString()
    val item = jsonSchemaValues.firstOrNull { it.friendly == passedValue }
      ?: error("Could not find a value in: $jsonSchemaValues that matches inbound of: $passedValue")
    return item as S
  }

  override fun serialize(encoder: Encoder, value: S) {
    val jsonSchemaEnumValue = value as? IJsonSchemaEnum<S> ?: error("Could not cast value to JSON Schema Enum.")
    encoder.encodeString(jsonSchemaEnumValue.friendly)
  }
}
