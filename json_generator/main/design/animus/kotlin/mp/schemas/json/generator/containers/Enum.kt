/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJsonSchemaEnum
import design.animus.kotlin.mp.schemas.json.core.serializers.AEnumSerializer
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.logger
import design.animus.kotlin.mp.schemas.json.generator.unSanitary
import kotlinx.serialization.Serializable

suspend fun createEnum(
  namePrefix: Option<String>,
  name: String,
  prop: PropertyTypes.Containers.EnumProperty,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  isDefinition: Boolean = false
): TypeName {
  logger.debug { "Creating enum with namePrefix: $namePrefix, name $name" }
  logger.debug { "Values are: ${prop.values}" }
  val packageName = if (isDefinition) "${config.packageBase}.definitions" else "${config.packageBase}.enums"
  val (partialName, requireSerial, serialName) = name.sanitizeForClassName()
  logger.debug { "Sanitized name: $name -> $partialName with serialName: $serialName" }
  val prefixName = when (namePrefix) {
    is Option.None -> "Enum${partialName.capitalize()}"
    is Option.Some -> {
      val (sanitiedNamePrefix, _, _) = namePrefix.item.sanitizeForClassName()
      "Enum${sanitiedNamePrefix}${partialName.capitalize()}"
    }
  }
  val className = if (isDefinition) prefixName.replace("Enum", "") else prefixName
  val file = FileSpec.builder(packageName, className)
  val baseEnum = TypeSpec.enumBuilder(className)

  val isUnSanitary = prop.values.any { it.unSanitary() }
  val typeName = if (isUnSanitary) {
    baseEnum.primaryConstructor(
      FunSpec.constructorBuilder()
        .addParameter("friendly", String::class)
        .build()
    )
      .addProperty(
        PropertySpec.builder("friendly", String::class)
          .initializer("friendly")
          .addModifiers(KModifier.OVERRIDE)
          .build()
      )
      .addAnnotation(
        AnnotationSpec.builder(Serializable::class)
          .addMember("with=%L", "${createObjectSerializerName(className)}::class")
          .build()
      )
      .addSuperinterface(
        IJsonSchemaEnum::class.toClassName()
          .parameterizedBy(ClassName(packageName, className))
      )
    prop.values.map { value ->
      baseEnum.addEnumConstant(
        value.sanitize().enumCleanup(),
        TypeSpec.anonymousClassBuilder()
          .addSuperclassConstructorParameter("%S", value)
          .build()
      )
    }
    val enumSerializer = createEnumSerializer(className, packageName)
    file.addType(enumSerializer)
    ClassName(packageName, className).copy(
      annotations = listOf(
        AnnotationSpec.builder(Serializable::class)
          .addMember("with=$packageName.${createObjectSerializerName(className)}::class")
          .build()
      )
    )
  } else {
    baseEnum.addAnnotation(Serializable::class)
    prop.values.map { value ->
      baseEnum.addEnumConstant(value)
    }
    ClassName(packageName, className)
  }
  if (requireSerial) baseEnum.addAnnotation(
    createSerialNameAnnotation(serialName)
  )
  file
    .addType(baseEnum.build())
    .build()
    .writeTo(config.outputPath)
  return typeName
}

suspend fun createEnumSerializer(
  className: String,
  packageName: String
): TypeSpec {
  val serializerName = createObjectSerializerName(className)
  return TypeSpec.objectBuilder(serializerName)
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .addSuperclassConstructorParameter("%L", "$className::values")
    .superclass(
      AEnumSerializer::class.toClassName()
        .parameterizedBy(
          ClassName(packageName, className)
        )
    )
    .build()
}
