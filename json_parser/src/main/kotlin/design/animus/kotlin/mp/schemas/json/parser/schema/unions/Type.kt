/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.logger
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class Type {
  @Serializable
  data class SingleType(val type: String) : Type()

  @Serializable
  data class Multiple(val types: List<String>) : Type()

  @Serializable
  object Empty : Type()

  @Serializer(forClass = Type::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<Type> {

    override fun serialize(encoder: Encoder, obj: Type) {
      when (obj) {
        is SingleType -> encoder.encodeSerializableValue(String.serializer(), obj.type)
        is Empty -> encoder.encodeNull()
        is Multiple -> encoder.encodeSerializableValue(ListSerializer(String.serializer()), obj.types)
      }
    }

    override fun deserialize(decoder: Decoder): Type = try {
      val possibleString = try {
        val x = decoder.decodeString()
        SingleType(x)
      } catch (e: Exception) {
        null
      }
      val possibleObject = try {
        val parsed = decoder.decodeSerializableValue(ListSerializer(String.serializer()))
        Multiple(parsed)
      } catch (e: Exception) {
        null
      }
      possibleObject ?: (possibleString ?: (possibleString ?: Empty))
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in Type: ${e.message}" }

      Empty
    }
  }
}
