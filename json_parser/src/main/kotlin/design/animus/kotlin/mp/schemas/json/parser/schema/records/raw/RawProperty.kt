/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.records.raw

import design.animus.kotlin.mp.schemas.json.parser.logger
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class RawProperty {

  @Serializable
  data class RawOfJsonSchemaProperty(val item: RawJSONSchemaProperty) : RawProperty()

  @Serializable
  data class RawOfBoolean(val item: Boolean) : RawProperty()

  @Serializable
  object RawOfEmpty : RawProperty()

  @Serializer(forClass = RawProperty::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<RawProperty> {
    override fun serialize(encoder: Encoder, obj: RawProperty) {
      when (obj) {
        is RawOfEmpty -> encoder.encodeNull()
        is RawOfJsonSchemaProperty -> encoder.encodeSerializableValue(
          RawJSONSchemaProperty.serializer(),
          obj.item
        )
        is RawOfBoolean -> encoder.encodeBoolean(obj.item)
      }
    }

    override fun deserialize(decoder: Decoder): RawProperty = try {
      logger.debug { "In RawProperty properties deserialize" }
      val possibleRawJSONSchemaProperty = try {
        RawOfJsonSchemaProperty(decoder.decodeSerializableValue(RawJSONSchemaProperty.serializer()))
      } catch (e: Exception) {
        logger.debug { "Raw Property is not of json schema property" }
        null
      }
      val possibleRawOfBoolean = try {
        RawOfBoolean(decoder.decodeBoolean())
      } catch (e: Exception) {
        logger.debug { "Raw Property is not of boolean property" }
        null
      }
      possibleRawJSONSchemaProperty ?: possibleRawOfBoolean ?: RawOfEmpty
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in RawProperty: ${e.message}" }
      RawOfEmpty
    }
  }
}
