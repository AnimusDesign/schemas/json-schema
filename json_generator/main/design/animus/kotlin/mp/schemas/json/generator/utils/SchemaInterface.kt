package design.animus.kotlin.mp.schemas.json.generator.utils

import com.squareup.kotlinpoet.ClassName
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.sanitizeForClassName

suspend fun GeneratorConfig.getSchemaInterfaceClassName(): ClassName {
  val (className, _, _) = this.schemaName.sanitizeForClassName()
  val finalName = "I$className"
  return ClassName(this.packageBase, finalName)
}
