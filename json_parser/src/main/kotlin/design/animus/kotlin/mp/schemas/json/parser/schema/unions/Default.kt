/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.logger
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class Default {
  @Serializable
  data class DefaultBoolean(val value: Boolean) : Default()

  @Serializable
  data class DefaultString(val value: String) : Default()

  @Serializable
  data class DefaultStringList(val value: List<String>) : Default()

  @Serializable
  data class DefaultNumber(val value: Int) : Default()

  @Serializable
  data class DefaultNumberList(val value: List<Int>) : Default()

  @Serializable
  object BlankObject : Default()

  @Serializable
  object Empty : Default()

  @Serializer(forClass = Default::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<Default> {
    override fun serialize(encoder: Encoder, value: Default) {
      when (value) {
        is Empty -> encoder.encodeNull()
        is DefaultBoolean -> encoder.encodeBoolean(value.value)
        is DefaultString -> encoder.encodeString(value.value)
        is DefaultStringList -> encoder.encodeSerializableValue(
          ListSerializer(String.serializer()),
          value.value
        )
        is DefaultNumber -> encoder.encodeInt(value.value)
        is DefaultNumberList -> encoder.encodeSerializableValue(ListSerializer(Int.serializer()), value.value)
        BlankObject -> encoder.encodeNull()
      }
    }

    override fun deserialize(decoder: Decoder): Default = try {
      logger.debug { "In Default deserializer for" }
      val possibleString = try {
        DefaultString(decoder.decodeString())
      } catch (e: Exception) {
        logger.debug { "Not String" }
        null
      }
      val possibleStringList = try {
        DefaultStringList(decoder.decodeSerializableValue(ListSerializer(String.serializer())))
      } catch (e: Exception) {
        logger.debug { "Not String List" }
        null
      }
      val possibleNumber = try {
        DefaultNumber(decoder.decodeInt())
      } catch (e: Exception) {
        logger.debug { "Not Number" }
        null
      }
      val possibleNumberList = try {
        DefaultNumberList(decoder.decodeSerializableValue(ListSerializer(Int.serializer())))
      } catch (e: Exception) {
        logger.debug { "Not Number List" }
        null
      }
      val possibleBoolean = try {
        DefaultBoolean(decoder.decodeBoolean())
      } catch (e: Exception) {
        logger.debug { "Not Boolean" }
        null
      }
      val possibleBlankObject = try {
        decoder.decodeSerializableValue(BlankObject.serializer())
      } catch (e: Exception) {
        logger.debug { "Not blank object." }
        null
      }
      when {
        possibleString != null -> {
          possibleString
        }
        possibleStringList != null -> {
          possibleStringList
        }
        possibleNumber != null -> {
          possibleNumber
        }
        possibleNumberList != null -> {
          possibleNumberList
        }
        possibleBoolean != null -> {
          possibleBoolean
        }
        possibleBlankObject != null -> {
          possibleBlankObject
        }
        else -> {
          Empty
        }
      }
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in Default: ${e.message}" }
//      e.printStackTrace()
      Empty
    }
  }
}
