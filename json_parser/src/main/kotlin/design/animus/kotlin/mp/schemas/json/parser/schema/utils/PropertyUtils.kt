/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.utils

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.platform.JSONSchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.ReferenceType
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.AdditionalProperties
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.ArrayItems
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Type
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.castToPropertyType
import mu.KotlinLogging

object PropertyUtils {
  private val logger = KotlinLogging.logger { }

  private suspend fun buildObjectProperties(
    schema: RawJSONSchemaProperty,
    definitions: List<JSONSchemaProperty>
  ) =
    schema.properties?.map { (prop, propSchema) ->
      logger.debug { "Making property: $prop" }
      PropertyTypes.Property.ObjectProperties(
        prop,
        getPropertyType(propSchema.type, propSchema.format, propSchema, definitions)
      )
    } ?: listOf()

  suspend fun getSingleType(
    typeName: String?,
    format: String?,
    schema: RawJSONSchemaProperty,
    definitions: List<JSONSchemaProperty>
  ): PropertyTypes {
    return if (typeName == "array" && schema.items != null) {
      logger.debug { "Found an array field" }
      val arrayType = when (schema.items) {
        null, ArrayItems.Empty -> PropertyTypes.Property.AnyProperty
        is ArrayItems.BooleanItems -> PropertyTypes.Property.AnyProperty
        is ArrayItems.ListItems -> PropertyTypes.Property.AnyOf(
          schema.items.items.map { rawItem ->
            when (rawItem) {
              is RawProperty.RawOfJsonSchemaProperty -> {
                val jsonSchemaProp = rawItem.item
                getPropertyType(
                  jsonSchemaProp.type,
                  jsonSchemaProp.format,
                  rawItem.item,
                  definitions
                )
              }
              is RawProperty.RawOfBoolean, RawProperty.RawOfEmpty -> PropertyTypes.Property.AnyProperty
            }
          }
        )
        is ArrayItems.ObjectItems -> {
          when (schema.items.items) {
            is RawProperty.RawOfJsonSchemaProperty -> {
              val jsonSchemaProp = schema.items.items.item
              getPropertyType(
                jsonSchemaProp.type,
                jsonSchemaProp.format,
                jsonSchemaProp,
                definitions
              )
            }
            is RawProperty.RawOfBoolean, RawProperty.RawOfEmpty -> PropertyTypes.Property.AnyProperty
          }
        }
      }
      PropertyTypes.Containers.Array(
        items = arrayType,
        minItems = if (schema.minItems == null) Option.None() else Option.Some(schema.minItems),
        maxItems = if (schema.maxItems == null) Option.None() else Option.Some(schema.maxItems),
        uniqueItems = schema.uniqueItems,
        description = if (schema.description != null) Option.Some(schema.description) else Option.None()
      )
    } else if (typeName == "array" && schema.items == null) {
      logger.debug { "Found an array of unspecified types" }
      PropertyTypes.Containers.Array(
        items = PropertyTypes.Property.AnyProperty,
        minItems = if (schema.minItems == null) Option.None() else Option.Some(schema.minItems),
        maxItems = if (schema.maxItems == null) Option.None() else Option.Some(schema.maxItems),
        uniqueItems = schema.uniqueItems,
        description = if (schema.description != null) Option.Some(schema.description) else Option.None()

      )
    } else if (typeName == "object") {
      val patternMap = schema.patternProperties.castToPropertyType(definitions)
      val objAddProps = schema.additionalProperties?.castToPropertyType(definitions)
      val objProps = buildObjectProperties(schema, definitions)
      PropertyTypes.Containers.Object(
        properties = if (objProps.isEmpty()) Option.None() else Option.Some(objProps),
        additionalProperties = if (objAddProps == null) Option.None() else Option.Some(objAddProps),
        pattern = if (patternMap.isEmpty()) Option.None() else Option.Some(patternMap),
        required = PropertyTypes.Property.Required.fromRawRequired(schema.required)
      )
    } else if (
      ((schema.type == null) || (schema.type == Type.Empty)) &&
      (schema.properties != null && schema.properties.isNotEmpty())
    ) {
      val patternMap = schema.patternProperties.map { (pattern, patternSchema) ->
        val patternType = when (patternSchema.type) {
          null -> getSingleType(pattern, patternSchema.format, patternSchema, definitions)
          else -> getPropertyType(patternSchema.type, patternSchema.format, patternSchema, definitions)
        }
        Pair(Regex(pattern), patternType)
      }.toMap()
      val objAddProps = when (val additional = schema.additionalProperties) {
        is AdditionalProperties.Bool -> null
        is AdditionalProperties.RawProperty -> {
          val subSchema = additional.inValue
          getPropertyType(subSchema.type, subSchema.format, subSchema, definitions)
        }
        AdditionalProperties.Empty, null -> null
      }
      PropertyTypes.Property.AnonymousObject(
        description = if (schema.description == null || schema.description.trim() == "" || schema.description.isEmpty() || schema.description.isNullOrBlank()) Option.None() else Option.Some(
          schema.description
        ),
        properties = if (schema.properties.isEmpty()) Option.None() else Option.Some(
          schema.properties.map { (k, v) ->
            PropertyTypes.Property.ObjectProperties(
              k,
              getPropertyType(v.type, v.format, v, definitions)
            )
          }
        ),
        required = PropertyTypes.Property.Required.fromRawRequired(schema.required),
        additionalProperties = if (objAddProps == null) Option.None() else Option.Some(objAddProps),
        pattern = if (patternMap.isEmpty()) Option.None() else Option.Some(patternMap)

      )
    } else if (schema.enum != null) {
      logger.info { "Found an enum" }
      PropertyTypes.Containers.EnumProperty(values = schema.enum)
    } else if (schema.oneOf.isNotEmpty()) {
      PropertyTypes.Property.OneOf(schema.oneOf.map { getPropertyType(it.type, it.format, it, definitions) })
    } else if (schema.anyOf.isNotEmpty()) {
      PropertyTypes.Property.AnyOf(
        schema.anyOf.map {
          getPropertyType(it.type, it.format, it, definitions)
        }
      )
    } else if (schema.allOf.isNotEmpty()) {
      PropertyTypes.Property.AllOf(schema.allOf.map { getPropertyType(it.type, it.format, it, definitions) })
    } else if (schema.ref != null) {
      logger.info { "Found reference to another schema" }
      val referenceType = when (schema.ref.contains("http")) {
        true -> ReferenceType.EXTERNAL
        false -> ReferenceType.INTERNAL
      }
      PropertyTypes.Property.Reference(
        referenceType,
        schema.ref,
        definition = if (referenceType == ReferenceType.INTERNAL) definitions.firstOrNull {
          it.propertyName == schema.ref.split(
            "/"
          ).last()
        } else null
      )
    } else if (schema.type == null && schema.format == null || (typeName!! == "null" && schema.format == null) || (typeName == null && schema.format == null)) {
      PropertyTypes.Property.AnyProperty
    } else {
      logger.debug { "Gathering property of $typeName and $format" }
      logger.debug { JSONSchemaTypes.values().size }
      val type = JSONSchemaTypes.values().firstOrNull {
        (it.typeName == typeName) && (it.format == format)
      } ?: throw Exception("Couldn't find type to match, type:$typeName format:$format")
      logger.debug { "Final type is: $type" }
      PropertyTypes.Property.BasicProperty(type)
    }
  }

  suspend fun getPropertyType(
    type: Type?,
    format: String?,
    schema: RawJSONSchemaProperty,
    definitions: List<JSONSchemaProperty>
  ): PropertyTypes {
    val propTypeResponse = when (type) {
      is Type.SingleType -> {
        logger.debug { "Received a single type." }
        getSingleType(type.type, schema.format, schema, definitions)
      }
      is Type.Multiple -> {
        logger.debug { "Received an any of type." }
        PropertyTypes.Property.AnyOf(
          type.types.map { getSingleType(it, schema.format, schema, definitions) }
        )
      }
      Type.Empty, null -> {
        logger.debug { "Received an empty type: $type" }
        logger.debug { "With schema: $schema" }
        getSingleType(null, schema.format, schema, definitions)
      }
    }
    logger.debug { "Cast as property type of: $propTypeResponse" }
    return propTypeResponse
  }
}
