/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.types

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.platform.JSONSchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Required as RawRequired

interface JSONSchemaUnion {
  val options: List<PropertyTypes>
}
typealias PropName = String

sealed class PropertyTypes {
  sealed class Containers : PropertyTypes() {
    data class Object(
      val properties: Option<List<Property.ObjectProperties>>,
      val additionalProperties: Option<PropertyTypes>,
      val pattern: Option<Map<Regex, PropertyTypes>>,
      val required: Option<Property.Required>,
      val description: Option<String> = Option.None()
    ) : Containers() {
      suspend fun isEmpty(): Boolean {
        return (
          this.properties.isNone() ||
            (this.properties.isSome() && this.properties.forceSome().isEmpty())
          ) &&
          this.additionalProperties.isNone() &&
          (
            this.pattern.isNone() ||
              (this.pattern.isSome() && this.pattern.forceSome().isEmpty())
            )
      }
    }

    data class Array(
      val items: PropertyTypes,
      val minItems: Option<Int>,
      val maxItems: Option<Int>,
      val uniqueItems: Boolean,
      val description: Option<String>
    ) : Containers()

    data class EnumProperty(val values: List<String>) : PropertyTypes()
  }

  sealed class Property : PropertyTypes() {
    data class BasicProperty(val property: JSONSchemaTypes) : PropertyTypes()
    data class Reference(
      val referenceType: ReferenceType,
      val reference: String,
      val definition: JSONSchemaProperty?
    ) : PropertyTypes()

    sealed class Required {
      data class Reference(val ref: String) : Required()
      data class ListOfStrings(val required: List<String>) : Required()
      companion object {
        suspend fun fromRawRequired(raw: RawRequired): Option<Required> {
          return when (raw) {
            is RawRequired.RequiredReference -> Option.Some(PropertyTypes.Property.Required.Reference(raw.value.ref!!))
            is RawRequired.RequiredStrings -> Option.Some(PropertyTypes.Property.Required.ListOfStrings(raw.value))
            RawRequired.Empty -> Option.None()
          }
        }
      }
    }

    data class OneOf(override val options: List<PropertyTypes>) : JSONSchemaUnion, PropertyTypes()
    data class AnyOf(override val options: List<PropertyTypes>) : JSONSchemaUnion, PropertyTypes()
    data class AllOf(override val options: List<PropertyTypes>) : JSONSchemaUnion, PropertyTypes()
    data class PatternMap(val patterns: Map<Regex, PropertyTypes>) : PropertyTypes()
    data class ObjectProperties(val name: PropName, val type: PropertyTypes)
    data class AnonymousObject(
      val description: Option<String>,
      val required: Option<Required>,
      val properties: Option<List<Property.ObjectProperties>>,
      val additionalProperties: Option<PropertyTypes>,
      val pattern: Option<Map<Regex, PropertyTypes>>
    ) : Property() {
      fun toObjectContainer(): PropertyTypes.Containers.Object {
        return Containers.Object(
          properties = this.properties,
          additionalProperties = this.additionalProperties,
          pattern = this.pattern,
          required = this.required,
          description = this.description
        )
      }
    }

    object AnyProperty : Property()
  }
}
