/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.drafts.v4

import design.animus.kotlin.mp.schemas.json.parser.schema.drafts.interfaces.IRawJsonSchemaDraftProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RawJSONSchemaPropertyV4(
  val type: Type? = null,
  val examples: List<String> = listOf(),
  @SerialName("\$ref")
  val ref: String? = null,
  val description: String? = "",
  val format: String? = null,
  val items: ArrayItems? = null,
  val properties: Map<String, RawJSONSchemaPropertyV4>? = null,
  val enum: List<String>? = null,
  val required: Required = Required.Empty,
  val patternProperties: Map<String, RawJSONSchemaPropertyV4> = mapOf(),
  val additionalProperties: AdditionalProperties? = null,
  val additionalItems: AdditionalItems? = null,
  val oneOf: List<RawJSONSchemaPropertyV4> = listOf(),
  val anyOf: List<RawJSONSchemaPropertyV4> = listOf(),
  val allOf: List<RawJSONSchemaPropertyV4> = listOf(),
  val minItems: Int? = null,
  val maxItems: Int? = null,
  val minProperties: Int? = null,
  val maxProperties: Int? = null,
  val uniqueItems: Boolean = false,
  val propertyNames: RawJSONSchemaPropertyV4? = null,
  val minLength: Int? = null,
  val default: Default = Default.Empty,
  val pattern: String? = null,
  @SerialName("const")
  val constant: String? = null,
  val not: RawJSONSchemaPropertyV4? = null,
  val exclusiveMinimum: Boolean = false,
  val minimum: PrimitiveIntOrProperty? = null,
  val exclusiveMaximum: Boolean = false,
  val maximum: PrimitiveIntOrProperty? = null
) : IRawJsonSchemaDraftProperty {

  override suspend fun toRawJsonSchemaProperty(): RawJSONSchemaProperty {
    return RawJSONSchemaProperty(
      type = this.type,
      examples = this.examples,
      ref = this.ref,
      description = this.description,
      format = this.format,
      items = this.items,
      properties = this.properties?.map { (key, value) ->
        key to value.toRawJsonSchemaProperty()
      }?.toMap(),
      enum = this.enum,
      required = this.required,
      patternProperties = this.patternProperties.map { (key, value) ->
        key to value.toRawJsonSchemaProperty()
      }.toMap(),
      additionalProperties = this.additionalProperties,
      additionalItems = this.additionalItems,
      oneOf = this.oneOf.map { it.toRawJsonSchemaProperty() },
      anyOf = this.anyOf.map { it.toRawJsonSchemaProperty() },
      allOf = this.allOf.map { it.toRawJsonSchemaProperty() },
      minItems = this.minItems,
      maxItems = this.maxItems,
      minProperties = this.minProperties,
      maxProperties = this.maxProperties,
      uniqueItems = this.uniqueItems,
      propertyNames = this.propertyNames?.toRawJsonSchemaProperty(),
      minLength = this.minLength,
      default = this.default,
      pattern = this.pattern,
      constant = this.constant,
      not = this.not?.toRawJsonSchemaProperty(),
      exclusiveMinimum = null,
      minimum = this.minimum,
      exclusiveMaximum = null,
      maximum = this.maximum,

    )
  }
}
