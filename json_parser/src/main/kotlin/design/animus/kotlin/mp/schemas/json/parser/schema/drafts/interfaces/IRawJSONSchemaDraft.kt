/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.drafts.interfaces

import design.animus.kotlin.mp.schemas.json.parser.schema.*
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Required
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Type

interface IRawJSONSchemaDraft<PropertyType> {
  val schema: String
  val type: Type?
  val id: String
  val title: String?
  val patternProperties: Map<String, PropertyType>
  val description: String?
  val properties: Map<String, PropertyType>
  val items: PropertyType?
  val required: Required
  val dependencies: Map<String, List<String>>
  val definitions: Map<String, PropertyType>

  suspend fun buildJSONObjectSchema(): JSONSchemaObject
  suspend fun buildJSONArraySchema(): JSONSchemaArray

  suspend fun schemaTypeDirector(schemaTypes: SchemaTypes): JSONSchema? {
    return when (schemaTypes) {
      SchemaTypes.OBJECT -> buildJSONObjectSchema()
      SchemaTypes.ARRAY -> buildJSONArraySchema()
      SchemaTypes.BOOLEAN -> null
    }
  }

  suspend fun toJsonSchema(): JSONSchema {
    val rsp = when (this.type) {
      is Type.SingleType -> {
        schemaTypeDirector((this.type as Type.SingleType).type.toSchemaTypes())
      }
      is Type.Multiple -> {
        val schemaType = (this.type as Type.Multiple).types.map {
          it.toSchemaTypes()
        }.first { it == SchemaTypes.OBJECT }
        schemaTypeDirector(schemaType)
      }
      Type.Empty, null -> throw Exception("Not supported schema type: ${this.type}")
    }
    return rsp!!
  }
}
