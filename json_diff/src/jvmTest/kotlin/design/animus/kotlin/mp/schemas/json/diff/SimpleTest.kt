package design.animus.kotlin.mp.schemas.json.diff

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable

@ExperimentalSerializationApi
class SimpleTest {
  @ExperimentalSerializationApi
  @Serializable
  data class Sample(
    val message: String,
    val fruit: String? = null
  ) : IJSONSchemaObjectBase {
    override fun getAJSONSchemaSerializer(): AJSONSchemaSerializer<IJSONSchemaObjectBase> {
      return SampleSerializer as AJSONSchemaSerializer<IJSONSchemaObjectBase>
    }
  }

  @ExperimentalSerializationApi
  object SampleSerializer : AJSONSchemaSerializer<Sample>(Sample.serializer()) {
    override val properties = listOf("message")
    override val additionalProperties = true
    override val patternProperty = false
    override val patternProperties = true
    override val regexes = setOf<Regex>()
    override val regexToType = mapOf<Regex, String>()
  }

  @Test
  fun testUpdate() = runBlocking {
    val old = Sample(message = "Hello", fruit = "banana")
    val new = Sample(message = "Goodbye", fruit = "banana")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    assertTrue("There is one change") { changes.size == 1 }
    val change = changes.first()
    assertTrue("Change action is update") { change.action == Actions.Update }
    assertTrue("Change path is 'message'") { change.path == "message" }
    assertTrue("Change old matches old struct") { change.old == old.message }
    assertTrue("Change new matches new struct") { change.new == new.message }
  }

  @Test
  fun testDelete() = runBlocking {
    val old = Sample(message = "Hello", fruit = "banana")
    val new = Sample(message = "Hello")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    assertTrue("There is one change") { changes.size == 1 }
    val change = changes.first()
    println(change)
    assertTrue("Change action is Delete") { change.action == Actions.Delete }
    assertTrue("Change path is 'fruit'") { change.path == "fruit" }
    assertTrue("Change old matches old struct") { change.old == old.fruit }
    assertTrue("Change new matches new struct") { change.new == null }
  }

  @Test
  fun testCreate() = runBlocking {
    val old = Sample(message = "Hello")
    val new = Sample(message = "Hello", fruit = "banana")
    val changes = diffJsonObjects(old, new) ?: error("Changes should not be null")
    assertTrue("There is one change") { changes.size == 1 }
    val change = changes.first()
    println(change)
    assertTrue("Change action is Create") { change.action == Actions.Create }
    assertTrue("Change path is 'fruit'") { change.path == "fruit" }
    assertTrue("Change old matches old struct") { change.old == null }
    assertTrue("Change new matches new struct") { change.new == new.fruit }
  }
}
