/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlin.reflect.KClass
import kotlinx.serialization.Serializable

internal suspend fun castRawType(typeRSP: TypeResponse): Pair<TypeName?, Option<out KClass<out Any>>> {
  return when (typeRSP) {
    TypeResponse.Empty -> {
      logger.warn { "Could not cast type/class name" }
      Pair(null, Option.None())
    }
    is TypeResponse.Name<*> -> {
      Pair(typeRSP.className, typeRSP.serializer)
    }
    is TypeResponse.Parameterized -> {
      Pair(typeRSP.parameterized, Option.None())
    }
  }
}

private suspend fun <R : Taggable> baseObjectPropertyHandler(
  props: List<PropertyTypes.Property.ObjectProperties>,
  requiredProps: List<String>,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None(),
  block: suspend (TypeName, PropertyTypes.Property.ObjectProperties, Boolean, Option<out KClass<out Any>>) -> R
): List<R> {
  return props.mapNotNull { prop ->
    logger.debug { "Processing property ${prop.name}" }
    val typeRSP = propertyToClass(prop.name, prop.type, config, namePrefix = namePrefix)
    logger.debug { "Property is: $typeRSP" }
    val nullable = !requiredProps.contains(prop.name)
    logger.debug { "Property nullable: is $nullable" }
    val (rawType, serializer) = castRawType(typeRSP)
    logger.debug { "Casting type too $rawType" }

    if (rawType != null) {
      val baseType = rawType.copy(nullable = nullable)
      block(baseType, prop, nullable, serializer)
    } else {
      logger.warn { "Could not process ${prop.name} as type is null." }
      null
    }
  }
}

suspend fun createObjPropertySpec(
  props: List<PropertyTypes.Property.ObjectProperties>,
  requiredProps: List<String>,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
) = baseObjectPropertyHandler(props, requiredProps, config, namePrefix = namePrefix) { type,
  prop, _, serializer ->
  val (propName, requireSerialName, serialName) = prop.name.sanitizeForPropName()
  val baseProp = PropertySpec.builder(propName, type)
    .initializer(propName)
  if (requireSerialName) baseProp.addAnnotation(
    createSerialNameAnnotation(serialName)
  )
  if (serializer.isSome()) {
    val ser = serializer.forceSome()
    baseProp
      .addAnnotation(
        AnnotationSpec
          .builder(Serializable::class)
          .addMember("with  = %T::class", ser)
          .build()
      )
  }
  baseProp.build()
}

suspend fun createObjParameterSpec(
  props: List<PropertyTypes.Property.ObjectProperties>,
  requiredProps: List<String>,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
) = baseObjectPropertyHandler(props, requiredProps, config, namePrefix = namePrefix) { type, prop, nullable, _ ->
  val (propName, _, _) = prop.name.sanitizeForPropName()
  val baseParam = ParameterSpec.builder(propName, type)
  if (nullable) baseParam.defaultValue("%L", null)
  baseParam.build()
}
