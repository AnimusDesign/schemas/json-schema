/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core.serializers

import kotlinx.datetime.TimeZone
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object TimeSerializer : KSerializer<design.animus.kotlin.mp.schemas.json.core.Time> {
  override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Time", PrimitiveKind.STRING)

  override fun serialize(encoder: Encoder, value: design.animus.kotlin.mp.schemas.json.core.Time) {
    encoder.encodeString("${value.hour}:${value.minute}:${value.second}+00:00")
  }

  override fun deserialize(decoder: Decoder): design.animus.kotlin.mp.schemas.json.core.Time {
    val (time, timeZone) = decoder.decodeString().split("+")
    val (hour, minute, second) = time.split(":")
    return design.animus.kotlin.mp.schemas.json.core.Time(
      hour.toInt(),
      minute.toInt(),
      second.toInt(),
      0,
      TimeZone.UTC
    )
  }
}
