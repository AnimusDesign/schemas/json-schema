/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.jsonschema

import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.platform.JSONSchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class PatternProperties : ISchemaTestBase {
  @Test
  fun testRead() = runBlocking {
    val content = loadTestSchema("patternproperties.json")
    val unsureSchema = JSONSchemaParser.parseJSON(content)
    assertTrue { unsureSchema is JSONSchemaObject }
    val schema = unsureSchema as JSONSchemaObject
    val correlationIds = schema.definitions.find { it.propertyName == "correlationIds" }
    assertTrue("Correlation ids is not null") { correlationIds != null }
    assertTrue("Correlation ids is an object") { correlationIds!!.type is PropertyTypes.Containers.Object }
    val correlationIdsType = correlationIds?.type as PropertyTypes.Containers.Object
    assertTrue("Correlation ids pattern map is not empty") { correlationIdsType.pattern.isSome() }
    val patternMap = correlationIdsType.pattern.forceSome()
    assertTrue("Correlation id has one pattern") {
      patternMap.size == 1
    }
    assertTrue("Correlation id pattern matches.") {
      patternMap.keys.first().pattern == """^[\w\d\.\-_]+${'$'}"""
    }
    assertTrue("Correlation pattern is one of") {
      val patternType = patternMap.values.first()
      patternType is PropertyTypes.Property.OneOf
    }

    val possibleReference = schema.definitions.find { it.propertyName == "Reference" }
    assertTrue("Reference is not null.") { possibleReference != null }
    val reference = possibleReference!!
    assertTrue { reference.type is PropertyTypes.Containers.Object }
    val refDef = reference.type as PropertyTypes.Containers.Object
    assertTrue { refDef.pattern.isSome() }
    val refPatternMap = refDef.pattern.forceSome()
    assertTrue("Reference only has one property.") {
      refPatternMap.size == 1
    }
    assertTrue("Reference regex matches.") {
      refPatternMap.keys.first().pattern == """[0-9]"""
    }
    assertTrue {
      val patternType = refPatternMap.values.first()
      patternType is PropertyTypes.Property.BasicProperty
    }
    val patternType = refPatternMap.values.first() as PropertyTypes.Property.BasicProperty
    assertTrue { patternType.property == JSONSchemaTypes.URIReference }
  }
}
