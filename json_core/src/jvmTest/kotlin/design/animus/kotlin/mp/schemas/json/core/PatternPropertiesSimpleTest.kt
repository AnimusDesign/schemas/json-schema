/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core

import design.animus.kotlin.mp.schemas.json.core.interfaces.*
import design.animus.kotlin.mp.schemas.json.core.serializers.AJSONSchemaSerializer
import design.animus.kotlin.mp.schemas.json.core.serializers.RegexSerializer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import org.junit.Test
import kotlin.test.assertTrue

val mapOneRegex = Regex("""^\w{3}-\d{3}""")
val mapTwoRegex = Regex("""item-\d{3}""")

class PatternPropertiesSimpleTest {
  private val logger = KotlinLogging.logger {}

  @Serializable
  sealed class MultipleMaps {

    @Serializable
    data class MapOne(
      override val map: Map<String, Int>,
      @Serializable(RegexSerializer::class)
      override val regex: Regex = mapOneRegex
    ) : IPatternMap<Int>, MultipleMaps() {
      companion object : IPatternMapUtils<Int> {
        override fun of(vararg pairs: Pair<String, Int>): MapOne {
          return MapOne(mapOf(*validatePairs(mapOneRegex, *pairs)))
        }
      }
    }

    @Serializable
    data class MapTwo(
      override val map: Map<String, String>,
      @Serializable(RegexSerializer::class)
      override val regex: Regex = mapTwoRegex
    ) : IPatternMap<String>, MultipleMaps() {

      companion object : IPatternMapUtils<String> {

        override fun of(vararg pairs: Pair<String, String>): MapTwo {
          return MapTwo(mapOf(*validatePairs(mapTwoRegex, *pairs)))
        }
      }
    }
  }

  @ExperimentalSerializationApi
  @Serializable
  data class Sample(
    val message: String,
    override val schemaAdditionalProperties: Map<String, Int>,
    override val schemaPatternProperties: Set<MultipleMaps>
  ) : IJSONSchemaObjectBase,
    IJSONSchemaObjectBaseWithAdditionalProperties<Int>,
    IJSONSchemaObjectBaseWithPatternProperties<MultipleMaps> {
    override fun getAJSONSchemaSerializer(): AJSONSchemaSerializer<IJSONSchemaObjectBase> {
      return SampleSerializer as AJSONSchemaSerializer<IJSONSchemaObjectBase>
    }
  }

  @ExperimentalSerializationApi
  object SampleSerializer : AJSONSchemaSerializer<Sample>(Sample.serializer()) {
    override val properties = listOf("message")
    override val additionalProperties = true
    override val patternProperty = false
    override val patternProperties = true
    override val regexes = setOf(mapOneRegex, mapTwoRegex)
    override val regexToType = mapOf(
      mapOneRegex to "design.animus.kotlin.mp.schemas.json.core.PatternPropertiesSimpleTest.MultipleMaps.MapOne",
      mapTwoRegex to "design.animus.kotlin.mp.schemas.json.core.PatternPropertiesSimpleTest.MultipleMaps.MapTwo"
    )
  }

  @ExperimentalSerializationApi
  @Test
  fun baseTest() {
    val data = Sample(
      "Hello",
      schemaAdditionalProperties = mapOf("abc" to 1),
      schemaPatternProperties = setOf(
        MultipleMaps.MapOne.of(
          "abc-123" to 1,
          "def-456" to 2
        ),
        MultipleMaps.MapTwo.of(
          "item-123" to "banana",
          "item-456" to "apple"
        )
      )
    )
    logger.info { data }
    val rsp = Json.encodeToString(SampleSerializer, data)
    logger.info { rsp }
    val obj = Json.decodeFromString(SampleSerializer, rsp)
    logger.info { obj }
    assertTrue("Contains one additional property") { obj.schemaAdditionalProperties.keys.size == data.schemaAdditionalProperties.keys.size }
    assertTrue("Pattern properties is set of two") { obj.schemaPatternProperties.size == data.schemaPatternProperties.size }
    val dataPatternProps = data.schemaPatternProperties.toList()
    val objPatternProps = obj.schemaPatternProperties.toList()
    val dataMapOne = dataPatternProps[0] as MultipleMaps.MapOne
    val objMapOne = objPatternProps[0] as MultipleMaps.MapOne
    assertTrue("First regex matches") {
      dataMapOne.regex.pattern == objMapOne.regex.pattern
    }
    assertTrue("First map matches") {
      dataMapOne.map == objMapOne.map
    }
  }

  @ExperimentalSerializationApi
  @Test
  fun propertiesDoNotMatchRegex() {
    val data = Sample(
      "Hello",
      schemaAdditionalProperties = mapOf("abc" to 1),
      schemaPatternProperties = setOf()
    )
    logger.info { data }
    val rsp = Json.encodeToString(SampleSerializer, data)
    logger.info { rsp }
    val obj = Json.decodeFromString(SampleSerializer, rsp)
    logger.info { obj }
    assertTrue("Contains one additional property") { obj.schemaAdditionalProperties.keys.size == data.schemaAdditionalProperties.keys.size }
    assertTrue("Pattern properties is set of zero") { obj.schemaPatternProperties.size == data.schemaPatternProperties.size }
  }
}
