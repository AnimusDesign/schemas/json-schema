/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.platform

import design.animus.functional.datatypes.option.Option
import kotlin.reflect.KClass

data class ClassRepresentation<S : Any>(
  val packageName: String,
  val className: String,
  val serializer: Option<KClass<S>> = Option.None()
)

fun <T : Any, S : Any> KClass<T>.toClassRepresentation(serializer: Option<KClass<S>> = Option.None()): ClassRepresentation<S> {
  val qualified = this.qualifiedName!!.split(".").toMutableList()
  val className = qualified.last()
  qualified.removeAt(qualified.size - 1)
  val packageName = qualified.joinToString(".")
  return ClassRepresentation(
    packageName = packageName,
    className = className,
    serializer
  )
}

data class PlatformRepresentation<S : Any>(
  val jvmType: ClassRepresentation<S>,
  val jsType: ClassRepresentation<S>,
  val linuxX64Type: ClassRepresentation<S>,
  val commonType: ClassRepresentation<S>
) {
  companion object {
    fun <S : Any> common(rep: ClassRepresentation<S>) = PlatformRepresentation(
      jvmType = rep,
      jsType = rep,
      linuxX64Type = rep,
      commonType = rep
    )
  }
}
