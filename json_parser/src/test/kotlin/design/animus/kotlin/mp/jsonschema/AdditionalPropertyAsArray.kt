/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.jsonschema

import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.platform.JSONSchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.junit.Test
import kotlin.test.assertTrue

class AdditionalPropertyAsArray : ISchemaTestBase {
  private val logger = KotlinLogging.logger { }

  @Test
  fun testRead() = runBlocking {
    logger.info { "Info" }
    logger.debug { "debug" }
    val content = loadTestSchema("additional-property-as-array.schema.json")
    val unsureSchema = JSONSchemaParser.parseJSON(content)
    assertTrue("Is a schema object") { unsureSchema is JSONSchemaObject }
    val schema = unsureSchema as JSONSchemaObject
    assertTrue("There is only one property") { schema.definitions.size == 1 }
    val element = schema.definitions.first()
    assertTrue("Element is named variables, and is object") {
      (element.propertyName == "variables") && (element.type is PropertyTypes.Containers.Object)
    }
    val addPropsObj = element.type as PropertyTypes.Containers.Object
    assertTrue("Object additional properties is set") { addPropsObj.additionalProperties.isSome() }
    assertTrue("Additional properties is any of") { addPropsObj.additionalProperties.forceSome() is PropertyTypes.Property.AnyOf }
    val anyOfProps = addPropsObj.additionalProperties.forceSome() as PropertyTypes.Property.AnyOf
    assertTrue("Contains expected property types") {
      anyOfProps.options.containsAll(
        listOf(PropertyTypes.Property.BasicProperty(JSONSchemaTypes.PlainString), PropertyTypes.Property.BasicProperty(JSONSchemaTypes.Int))
      )
    }
  }
}
