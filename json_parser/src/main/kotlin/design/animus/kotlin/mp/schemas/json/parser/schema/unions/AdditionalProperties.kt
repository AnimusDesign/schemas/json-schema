/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.utils.PropertyUtils
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class AdditionalProperties {
  @Serializable
  data class Bool(val inValue: Boolean) : AdditionalProperties()

  @Serializable
  data class RawProperty(val inValue: RawJSONSchemaProperty) : AdditionalProperties()

  @Serializable
  object Empty : AdditionalProperties()

  suspend fun castToPropertyType(definitions: Definitions) = when (this) {
    is AdditionalProperties.Bool -> {
      when (this.inValue) {
        true -> PropertyTypes.Property.AnyProperty
        false -> null
      }
    }
    is AdditionalProperties.RawProperty -> {
      val subSchema = this.inValue
      PropertyUtils.getPropertyType(subSchema.type, subSchema.format, subSchema, definitions)
    }
    AdditionalProperties.Empty -> null
  }

  @Serializer(forClass = AdditionalProperties::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<AdditionalProperties> {
    override fun serialize(encoder: Encoder, obj: AdditionalProperties) {
      when (obj) {
        is Bool -> encoder.encodeSerializableValue(Bool.serializer(), obj)
        is RawProperty -> encoder.encodeSerializableValue(RawProperty.serializer(), obj)
        is Empty -> encoder.encodeNull()
      }
    }

    override fun deserialize(decoder: Decoder): AdditionalProperties = try {
      logger.debug { "In additional properties deserialize" }
      val possibleBool = try {
        val x = decoder.decodeBoolean()
        Bool(x)
      } catch (e: Exception) {
        logger.debug { "Additional properties is not a boolean." }
        null
      }
      val possibleString = try {
        val x = decoder.decodeString()
        Bool(x.toBoolean())
      } catch (e: Exception) {
        logger.debug { "Additional properties is not a string." }
        null
      }
      val possibleObject = try {
        RawProperty(decoder.decodeSerializableValue(RawJSONSchemaProperty.serializer()))
      } catch (e: Exception) {
        logger.debug { "Additional properties is not an object." }
        null
      }
      possibleBool ?: possibleObject ?: possibleString ?: Empty
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in AdditionalProperties: ${e.message}" }
      Empty
    }
  }
}
