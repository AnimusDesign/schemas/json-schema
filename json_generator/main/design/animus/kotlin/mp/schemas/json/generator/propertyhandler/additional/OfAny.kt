/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.additional

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.records.ObjectResponse
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.serialization.json.JsonElement

suspend fun additionalPropertiesOfAny(
  schema: PropertyTypes.Containers.Object,
  type: PropertyTypes,
  nestedNamePrefix: Option<String>,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
): Option<ObjectResponse> = additionalPropertiesBase(schema, type, config) {
  Option.Some(JsonElement::class.toClassName())
}
