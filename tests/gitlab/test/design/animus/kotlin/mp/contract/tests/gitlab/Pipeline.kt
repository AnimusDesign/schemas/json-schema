package design.animus.kotlin.mp.contract.tests.gitlab

import design.animus.kotlin.mp.contract.simple.address.Gitlab
import design.animus.kotlin.mp.contract.simple.address.GitlabSerializer
import design.animus.kotlin.mp.contract.simple.address.definitions.Job
import design.animus.kotlin.mp.contract.simple.address.definitions.JobTemplate
import design.animus.kotlin.mp.contract.simple.address.definitions.Variables
import design.animus.kotlin.mp.contract.simple.address.anonymous.Services
import design.animus.kotlin.mp.contract.simple.address.unions.anyof.AnyOfJobTemplateScript
import design.animus.kotlin.mp.contract.simple.address.unions.anyof.AnyOfVariablesSchemaAdditionalProperties
import design.animus.kotlin.mp.contract.simple.address.unions.oneof.OneOfJobTemplateScript
import design.animus.kotlin.mp.contract.simple.address.unions.oneof.OneOfServices
import kotlin.test.assertTrue
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import mu.KotlinLogging
import org.junit.Test

internal val logger = KotlinLogging.logger {}

@ExperimentalSerializationApi
class Pipeline {
  @Test
  fun testPipelineSerialize() {
    val pipeLine = Gitlab(
      stages = listOf("build", "test"),
      schemaAdditionalProperties = mapOf(
        "build:all:" to Job.PossibleJobTemplate(
          JobTemplate(
            services = listOf(
              OneOfServices.Possible1(
                Services(
                  name = "postgres:12.0",
                  alias = "db"
                )
              ),
              OneOfServices.PossibleString("docker:dind")
            ),
            script = OneOfJobTemplateScript.Possible1(
              item = listOf(
                AnyOfJobTemplateScript.PossibleString("./gradlew assemble")
              )
            ),
            variables = Variables(
              schemaAdditionalProperties = mapOf(
                "USER" to AnyOfVariablesSchemaAdditionalProperties.PossibleString("admin"),
                "TOKEN" to AnyOfVariablesSchemaAdditionalProperties.PossibleString("abc123"),
                "PORT" to AnyOfVariablesSchemaAdditionalProperties.PossibleInteger(80)
              )
            )
          )
        )
      )
    )
    println(pipeLine)
    val raw = Json.encodeToJsonElement(GitlabSerializer, pipeLine) as? JsonObject ?: error("Should be json object")
    println(raw)

    val stages = raw["stages"] as? JsonArray ?: error("Stages was null but shouldn not be.")
    assertTrue("Stages contains build") { stages.contains(JsonPrimitive("build")) }
    assertTrue("Stages contains test") { stages.contains(JsonPrimitive("test")) }

    val job = raw["build:all:"] as? JsonObject ?: error("Job Template Should Not be empty")
	println(job)
    val jobServices = job["services"] as? JsonArray ?: error("Services should be an array")
    assertTrue("Job should contain two services.") { jobServices.size == 2 }
    assertTrue("First Service is PSQL") {
      val psql = jobServices[0] as? JsonObject ?: error("First service should be JSON Object")
      val jsonName = psql["name"] as JsonPrimitive ?: error("name should be present")
      val jsonAlias = psql["alias"] as JsonPrimitive ?: error("alias should be present")
      jsonAlias.isString && jsonName.isString &&
        (jsonAlias.content == "db") &&
        (jsonName.content == "postgres:12.0")
    }
    assertTrue("Second service is Docker in Docker") {
      val docker = jobServices[1] as? JsonPrimitive ?: error("Docker service should be primitive")
      docker.isString && (docker.content == "docker:dind")
    }

    val variables = job["variables"] as? JsonObject ?: error("Variables should be a json object")
    assertTrue("User is present") {
      val jsonUser = variables["USER"] as? JsonPrimitive ?: error("user should be json primtive")
      jsonUser.isString && (jsonUser.content == "admin")
    }
    assertTrue("Port is present") {
      val jsonPort = variables["PORT"] as? JsonPrimitive ?: error("Port should be primitive")
      println(jsonPort)
      !jsonPort.isString && (jsonPort.content == "80")
    }
    assertTrue("Token is present") {
      val jsonToken = variables["TOKEN"] as? JsonPrimitive ?: error("Port should be primitive")
      println(jsonToken)
      jsonToken.isString && (jsonToken.content == "abc123")
    }
  }
}
