/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import design.animus.kotlin.contract.Versions.Dependencies

plugins {
  id("java-gradle-plugin")
}

val artifactID = "json_generator"
kotlin {
  target {

    mavenPublication {
      artifactId = artifactID
    }
  }
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main")) // , "generated"))
      dependencies {
        api(project(":json_core:"))
        implementation(project(":json_parser:"))
        implementation("com.benasher44:uuid:${Dependencies.mpUUID}")
        implementation("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("io.ktor:ktor-client-core:${Dependencies.ktor}")
        implementation("io.ktor:ktor-client-cio:${Dependencies.ktor}")
        implementation("com.squareup.okhttp3:okhttp:${Dependencies.okHTTP}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      resources.setSrcDirs(listOf("src/test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("build") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
  kotlinOptions.apiVersion = "1.4"
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      this.groupId = group.toString()
      this.version = version
      artifactId = artifactID
      from(components["java"])
    }
  }
}

tasks.withType<Test>().configureEach {
  useJUnit()
  reports.junitXml.isEnabled = true
}

gradlePlugin {
  plugins {
    create("jsonSchemaGenerator") {
      id = "design.animus.kotlin.mp.schemas.json.generator"
      implementationClass = "design.animus.kotlin.mp.schemas.json.generator.JsonSchemaGenerator"
    }
  }
}
