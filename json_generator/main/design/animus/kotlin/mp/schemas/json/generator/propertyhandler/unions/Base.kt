/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.unions

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.containers.createObjectSerializerName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.JSONSchemaUnion
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.serialization.*
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

fun buildNestedClassDeserializer(parent: String, name: String): String {
  return """
        val possible$name = try {
            $parent.$name(item=Json.decodeFromJsonElement(${name.decapitalize()}Serializer, finalObj))
        } catch (e:Exception) {
            null
        }
  """.trimIndent()
}

@ExperimentalSerializationApi
suspend fun createUnionBase(
  parentName: String,
  prop: JSONSchemaUnion,
  config: GeneratorConfig,
  prefix: String,
  wrapper: UnionWrapper = UnionWrapper.None,
  wrapperPosition: UnionWrapperPosition = UnionWrapperPosition.External,
  inPackageName: Option<String> = Option.None()
): TypeName = try {
  val packageName = when (inPackageName) {
    is Option.Some -> inPackageName.item
    is Option.None -> "${config.packageBase}.unions.${prefix.toLowerCase()}"
  }
  val (partialName, requireSerial, serialName) = parentName.sanitizeForClassName()
  val className = if (packageName.contains("definitions")) {
    partialName.capitalize()
  } else {
    "${prefix}${partialName.capitalize()}"
  }
  val parent = TypeSpec.classBuilder(className)
    .addModifiers(KModifier.SEALED)
    .addAnnotation(Serializable::class)
    .addAnnotation(experimentalSerializationClass)
  if (requireSerial) parent.addAnnotation(
    createSerialNameAnnotation(serialName)
  )
  val file = fileSpecBootStrap(packageName, className)
    .addImport("kotlinx.serialization.builtins", "ListSerializer")
    .addImport("kotlinx.serialization.builtins", "serializer")
  val parentClassRef = ClassName(packageName, className)
  val nested = prop.options.mapIndexed { index, t ->
    logger.info { "Received nested option of: $t" }
    val (prefixName, subClassName) = when (t) {
      is PropertyTypes.Property.Reference -> {
        val reference = t.reference.split("/").last()
        val (refName, _, _) = reference.sanitizeForClassName()
        Pair(Option.Some(refName), "Possible$refName")
      }
      is PropertyTypes.Property.BasicProperty -> {
        val simpleName = t.property.typeName.capitalize()
        Pair(Option.Some(simpleName), "Possible$simpleName")
      }
      is PropertyTypes.Property.AnonymousObject -> {
        if (t.description.isSome() && (
          t.description.forceSome().isNotEmpty() && t.description.forceSome()
            .isNotBlank() && t.description.forceSome() != null
          )
        ) {
          val name = t.description.forceSome().split(" ").joinToString("") { it.capitalize() }
          Pair(Option.Some(name), name)
        } else {
          Pair(Option.None<String>(), "Possible$index")
        }
      }
      is PropertyTypes.Containers.EnumProperty -> {
        Pair(Option.Some("${parentClassRef.simpleName}$index"), "Possible$index")
      }
      else -> {
        Pair(Option.None<String>(), "Possible$index")
      }
    }
    logger.info { "Set Prefix name to: $prefixName" }
    val typeResponse = propertyToClass(parentName, t, config, namePrefix = prefixName)
    val (nestedClassType, nestedSerializer) = buildNestedClass(subClassName, parentClassRef, typeResponse, wrapper, wrapperPosition, config)
    parent.addType(nestedClassType)
    file.addProperty(nestedSerializer)
    subClassName
  }
  val componentSerializer = TypeSpec.objectBuilder(createObjectSerializerName(className))
    .addAnnotation(
      AnnotationSpec
        .builder(Serializer::class)
        .addMember("forClass = $className::class")
        .build()
    )
    .addAnnotation(experimentalSerializationClass)
    .addSuperinterface(
      ClassName("kotlinx.serialization", "KSerializer")
        .parameterizedBy(ClassName(packageName, className))
    )
    .addFunction(
      FunSpec.builder("deserialize")
        .addModifiers(KModifier.OVERRIDE)
        .addParameter("decoder", Decoder::class)
        .addCode(
          """
                val input = decoder as? %T ?: throw %T(%S)
                val rawObj = input.decodeJsonElement()
                            as? %T ?:
                            throw %T(%S)
                val finalObj = %T(mapOf("item" to rawObj))
                val stub = %T::class
                ${nested.joinToString("\n") { buildNestedClassDeserializer(className, it) }}
                return ${nested.joinToString(" ?: ") { "possible$it" }}
                ?: throw Exception("Could not deserialize")
          """,
          JsonDecoder::class, SerializationException::class, "Expected Json Input",
          JsonElement::class, SerializationException::class, "Didn't find json object",
          JsonObject::class, Json::class
        )
        .returns(ClassName(packageName, className))
        .build()
    )
    .addFunction(
      FunSpec.builder("serialize")
        .addModifiers(KModifier.OVERRIDE)
        .addParameter("encoder", Encoder::class)
        .addParameter("value", ClassName(packageName, className))
        .addCode(
          """
                when (value) {
                ${
          nested.joinToString("\n") {
            "     is $className.$it -> encoder.encodeSerializableValue(${it.decapitalize()}Serializer, value.item)"
          }
          }
                }

          """.trimIndent()
        )
        .build()
    )
  file
    .addType(componentSerializer.build())
    .addType(parent.build())
    .build()
    .writeTo(config.outputPath)
  val finalClass = parentClassRef.copy(
    annotations = listOf(
      createCustomSerializer(ClassName(packageName, createObjectSerializerName(className)))
    )
  )
  if (wrapperPosition is UnionWrapperPosition.Internal) {
    finalClass
  } else {
    val wrapperClass = wrapper.wrapperClass
    when (wrapper) {
      UnionWrapper.PropertyMap -> wrapperClass.parameterizedBy(ClassName("kotlin", "string"), finalClass)
      UnionWrapper.PropertyArray -> wrapperClass.parameterizedBy(finalClass)
      UnionWrapper.None -> finalClass
    }
  }
} catch (e: Exception) {
  logger.info { "Encounter unexpected exception in union base: ${e.message}" }
  e.printStackTrace()
  throw e
}
