/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator

import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.TypeName
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropName
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.utils.createSerialName
import kotlin.reflect.KClass
import kotlinx.serialization.Serializable

typealias PackageName = String
typealias ClazzName = String

suspend fun <T : Any> KClass<T>.toImportStatement(): Pair<PackageName, ClazzName> {
  val qualified = this.qualifiedName!!.split(".").toMutableList()
  val className = qualified.last()
  qualified.removeAt(qualified.size - 1)
  val packageName = qualified.joinToString(".")
  return Pair(
    packageName,
    className
  )
}

val experimentalSerializationClass = ClassName("kotlinx.serialization", "ExperimentalSerializationApi")

suspend fun <T : Any> KClass<T>.toClassName(): ClassName {
  val (packageName, clazz) = this.toImportStatement()
  return ClassName(packageName, clazz)
}

data class SanitizedClassName(
  val className: String,
  val requireSerialName: Boolean,
  val serialName: String
)

suspend fun String.sanitizeForItemName(isProp: Boolean): SanitizedClassName {
  val requireSerialName = illegalCharacters.any { this.contains(it) }
  val sanitizedName = this.split(*illegalCharacters.toTypedArray()).mapIndexed { index, value ->
    if (index == 0 && isProp) {
      value
    } else {
      value.capitalize()
    }
  }.joinToString("")
  return SanitizedClassName(
    className = sanitizedName,
    requireSerialName = requireSerialName,
    serialName = this
  )
}

suspend fun String.sanitizeForClassName() = this.sanitizeForItemName(false)

suspend fun String.sanitizeForPropName() = this.sanitizeForItemName(true)

internal val illegalCharacters = listOf(".", "`", "#", "-", ":", "_", """$""", "<", ">", "/", "\"", "'")
internal val charactersThatRequireEscape = listOf("\$")

internal fun String.unSanitary() = illegalCharacters.map { this.contains(it) }.contains(true)

suspend fun createSerialNameAnnotation(serialName: String) =
  AnnotationSpec.builder(kotlinx.serialization.SerialName::class)
    .addMember("\"%L\"", serialName.escapeIllegalCharacters())
    .build()

suspend fun PropName.toSerialNameAnnotation() = createSerialNameAnnotation(this.createSerialName())

suspend fun createCustomSerializer(type: TypeName) = AnnotationSpec
  .builder(Serializable::class)
  .addMember("with = %T::class", type)
  .build()

internal suspend fun String.determinePackageAndClassName(default: () -> Pair<String, String>): Pair<String, String> {
  return if (this.contains(".")) {
    val split = this.split(".")
    Pair(
      split.subList(0, split.size - 1).joinToString("."),
      split.last()
    )
  } else {
    default()
  }
}

suspend fun fileSpecBootStrap(packageName: String, className: String): FileSpec.Builder {
  val file = FileSpec.builder(packageName, className)
    .addComment("File is generated do not hand edit.")
  return file
}

suspend fun JSONSchemaObject.definitionRequiresSerializer(refClass: String): Boolean {
  val def = this.definitions.firstOrNull {
    it.propertyName == refClass.trim()
  }
    ?: throw IllegalArgumentException("Could not find reference class: $refClass")
  return when (def.type) {
    is PropertyTypes.Containers.Object -> {
      !(def.type as PropertyTypes.Containers.Object).isEmpty()
    }
    is PropertyTypes.Property.AnonymousObject,
    PropertyTypes.Property.AnyProperty,
    is PropertyTypes.Property.OneOf,
    is PropertyTypes.Property.AnyOf,
    is PropertyTypes.Property.AllOf,
    is PropertyTypes.Property.PatternMap -> true
    is PropertyTypes.Containers.Array,
    is PropertyTypes.Containers.EnumProperty,
    is PropertyTypes.Property.BasicProperty,
    is PropertyTypes.Property.Reference -> false
  }
}

suspend fun JSONSchemaObject.getReference(refClass: String): JSONSchemaProperty {
  val def = this.definitions.firstOrNull {
    it.propertyName == refClass.trim()
  }
    ?: throw IllegalArgumentException("Could not find reference class: $refClass")
  return def
}

internal fun String.baseSanitize(
  charList: List<String> = illegalCharacters,
  block: (String) -> String = { "" }
): String {
  var finalString = this
  charList.forEach {
    finalString = finalString.replace(it, block(it))
  }
  return finalString
}

internal fun String.sanitize() = baseSanitize()
internal fun String.escapeIllegalCharacters() = baseSanitize(charList = charactersThatRequireEscape) {
  """\$it"""
}

suspend fun mutateNameIfEnabled(
  name: String,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
): SanitizedClassName {
  return if (config.mutateObjectName.isSome()) {
    val renameContext = design.animus.kotlin.mp.schemas.json.generator.RenameContext(
      packageName = config.packageBase,
      itemName = name
    )
    logger.debug { "Mutate name exists: executing with rename context of:" }
    val (_, partialRequireSerialName, partialSerialName) = name.sanitizeForClassName()
    val result = config.mutateObjectName.forceSome()(renameContext)
    logger.debug { "Mutate name result is: $result" }
    SanitizedClassName(result, partialRequireSerialName, partialSerialName)
  } else {
    logger.debug { "Mutate name not present, defaulting to: $name" }
    name.sanitizeForClassName()
  }
}

suspend fun mutatePackageNameIfEnabled(
  packageName: String,
  itemName: String,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
): String {
  val packageNameIllegalChars = illegalCharacters.filter { it != "." }
  val outName = if (config.mutatePackageName.isSome()) {
    val renameContext =
      design.animus.kotlin.mp.schemas.json.generator.RenameContext(packageName = packageName, itemName = itemName)
    logger.debug { "Mutate package name exists: executing with rename context of $renameContext" }
    val result = config.mutatePackageName.forceSome()(renameContext)
    logger.debug { "Result of mutation is: $result" }
    result
  } else {
    logger.debug { "Mutate package name not present, defaulting to: $packageName" }
    packageName
  }
  return outName.baseSanitize(charList = packageNameIllegalChars).toLowerCase()
}

val reservedKeywords = listOf("String", "Int", "Number", "Boolean", "Double", "Float", "Short")
suspend fun String.enumCleanup(): String {
  var rsp = this
  val prefix = "Of"
  if (this.matches(Regex("""^\d+"""))) {
    rsp = "$prefix$this"
  }
  if (reservedKeywords.any { rsp == it }) {
    rsp = "$prefix$this"
  }
  return rsp
}
