/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.jsonschema

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.SchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.platform.JSONSchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class SchemaMetaTest : ISchemaTestBase {
  @Test
  fun testRead() = runBlocking {
    val content = loadTestSchema("schema.meta.json")
    val unsureSchema = JSONSchemaParser.parseJSON(content)
    assertTrue { unsureSchema is JSONSchemaObject }
    val schema = unsureSchema as JSONSchemaObject
    assertTrue("Schema id matches.") { schema.id == "http://json-schema.org/draft-07/schema#" }
    assertTrue("Schema is of type object") { schema.type == SchemaTypes.OBJECT }

    assertTrue("Schema properties contains description") { schema.properties.any { it.propertyName == "description" } }
    val definitions = schema.properties.find { it.propertyName == "description" }
      ?: error("Failed to find 'description' in schema/properties")
    assertTrue("Schema/properties/description is of type string") {
      definitions.type is PropertyTypes.Property.BasicProperty &&
        (definitions.type as PropertyTypes.Property.BasicProperty).property == JSONSchemaTypes.PlainString
    }

    assertTrue("Schema properties contains description") { schema.properties.any { it.propertyName == "enum" } }
    val enumDef = schema.properties.find { it.propertyName == "enum" }
      ?: error("Failed to find 'enum' in schema/properties")
    assertTrue("Enum is of type array") { enumDef.type is PropertyTypes.Containers.Array }
    val enumArrayDef = enumDef.type as PropertyTypes.Containers.Array
    assertTrue("Enum definition for array/minimum items is 1") {
      enumArrayDef.minItems is Option.Some &&
        enumArrayDef.minItems.forceSome() == 1
    }
    assertTrue("Enum definition for array/maximum items is 1") {
      enumArrayDef.maxItems is Option.None
    }
    assertTrue("Enum definition for array/unique items is true") {
      enumArrayDef.uniqueItems
    }

    assertTrue("Schema definitions contains stringArray") { schema.definitions.any { it.propertyName == "simpleTypes" } }
    val simpleTypes = schema.definitions.find { it.propertyName == "simpleTypes" }
      ?: error("Failed to find 'simpleTypes' in schema/definitions")
    assertTrue("Schema/definition simpleTypes is Enum") {
      simpleTypes.type is PropertyTypes.Containers.EnumProperty
    }
    val simpleTypesEnumDef = simpleTypes.type as PropertyTypes.Containers.EnumProperty
    assertTrue("Schema/definition simpleTypes enum contains expected values") {
      simpleTypesEnumDef.values.containsAll(
        listOf(
          "array",
          "boolean",
          "integer",
          "null",
          "number",
          "object",
          "string"
        )
      )
    }

    val constProp = schema.properties.firstOrNull { it.propertyName == "const" }
      ?: error("Could not find property definition of const")
    assertTrue("schema/properties/const is AnyProperty") { constProp.type is PropertyTypes.Property.AnyProperty }
  }
}
