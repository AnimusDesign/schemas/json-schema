/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import design.animus.kotlin.contract.Versions.Dependencies
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig

plugins {
  id("design.animus.kotlin.mp.schemas.json.generator")
}

kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main", "generated"))
      dependencies {
        implementation(project(":json_core:"))
        implementation(project(":json_parser:"))
        implementation(project(":json_generator:"))
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      kotlin.setSrcDirs(mutableListOf("test"))
      resources.setSrcDirs(listOf("test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("build") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      this.groupId = group.toString()
      this.version = version
      artifactId = "kotlin-mp-contract-asyncapi"
      from(components["java"])
    }
  }
}

tasks.withType<Test>().configureEach {
  useJUnit()
  reports.junitXml.isEnabled = true
}

JsonSchemaGeneratorConfig {
  schema = listOf(
    GeneratorConfig(
      packageBase = "design.animus.kotlin.mp.contract.simple.address",
      outputPath = File("$projectDir/generated"),
      schemaFile = File("$projectDir/main/resources/gitlab-ci.schema.json"),
      schemaName = "Gitlab"
    )
  )
}
