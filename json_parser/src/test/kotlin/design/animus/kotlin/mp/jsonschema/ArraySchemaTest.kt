/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.jsonschema

import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.SchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlin.test.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Test

class ArraySchemaTest : ISchemaTestBase {
  @Test
  fun testRead() = runBlocking {
    val content = loadTestSchema("array.schema.json")
    val unsureSchema = JSONSchemaParser.parseJSON(content)
    assertTrue { unsureSchema is JSONSchemaObject }
    val schema = unsureSchema as JSONSchemaObject
    assertTrue { schema.id == "https://example.com/array.schema.json" }
    assertTrue { schema.description == "An address similar to http://microformats.org/wiki/h-card" }
    assertTrue { schema.type == SchemaTypes.OBJECT }
    println(schema.properties)
    assertTrue {
      val element = schema.properties.find { it.propertyName == "users" }
      (element != null) && (element.type is PropertyTypes.Containers.Array) && ((element.type as PropertyTypes.Containers.Array).items is PropertyTypes.Property.BasicProperty)
    }
    assertTrue {
      val element = schema.properties.find { it.propertyName == "favorites" }
      (element != null) && (element.type is PropertyTypes.Containers.Array) && ((element.type as PropertyTypes.Containers.Array).items is PropertyTypes.Containers.Object)
    }
  }
}
