/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.drafts.v7

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.parser.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaArray
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.SchemaTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.drafts.interfaces.IRawJSONSchemaDraft
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.AdditionalProperties
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Required
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.Type
import design.animus.kotlin.mp.schemas.json.parser.schema.unions.castToPropertyType
import design.animus.kotlin.mp.schemas.json.parser.schema.utils.PropertyUtils
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RawJSONSchemaV7(
  @SerialName("\$schema")
  override val schema: String,
  override val type: Type? = Type.SingleType(SchemaTypes.OBJECT.friendly),
  @SerialName("\$id")
  override val id: String = "",
  override val title: String = "",
  val additionalProperties: AdditionalProperties? = null,
  override val patternProperties: Map<String, PossiblePropertyV7> = mapOf(),
  override val description: String = "",
  override val properties: Map<String, PossiblePropertyV7> = mapOf(),
  override val items: PossiblePropertyV7? = null,
  override val required: Required = Required.Empty,
  override val dependencies: Map<String, List<String>> = mapOf(),
  override val definitions: Map<String, PossiblePropertyV7> = mapOf()
) : IRawJSONSchemaDraft<PossiblePropertyV7> {
  override suspend fun buildJSONObjectSchema(): JSONSchemaObject {
    val definitions = this.definitions.map { (name, possibleSchema) ->
      logger.info { "Attempting to make definition for: $name of: $schema" }
      val propType = when (possibleSchema) {
        is PossiblePropertyV7.OfBoolean -> {
          logger.debug { "Property is of type boolean casting to Any Property" }
          when (possibleSchema.item) {
            true -> PropertyTypes.Property.AnyProperty
            false -> null
          }
        }
        is PossiblePropertyV7.OfProperty -> {
          logger.debug { "Property has definition determining property type." }
          val schema = possibleSchema.item
          val propType = PropertyUtils.getPropertyType(
            schema.type,
            schema.format,
            schema.toRawJsonSchemaProperty(),
            listOf()
          )
          logger.debug { "Determined property type is: $propType" }
          propType
        }
        PossiblePropertyV7.OfEmpty -> PropertyTypes.Property.AnyProperty
      }
      val definition = JSONSchemaProperty(
        propertyName = name,
        type = propType ?: PropertyTypes.Property.AnyProperty
      )
      logger.debug { "Created definition of: $definition" }
      definition
    }
    val addProps = this.additionalProperties?.castToPropertyType(definitions)
    val patternProps = this.patternProperties
      .filter { (_, value) -> value !is PossiblePropertyV7.OfProperty }
      .map { (key, value) ->
        key to (value as PossiblePropertyV7.OfProperty).item.toRawJsonSchemaProperty()
      }
      .toMap()
      .castToPropertyType(definitions)
    return JSONSchemaObject(
      id = this.id,
      schema = this.schema,
      type = SchemaTypes.OBJECT,
      description = this.description,
      properties = this.properties.map { (name, inSchema) ->
        val propType = when (inSchema) {
          is PossiblePropertyV7.OfBoolean -> PropertyTypes.Property.AnyProperty
          is PossiblePropertyV7.OfProperty -> {
            val schema = inSchema.item
            PropertyUtils.getPropertyType(
              schema.type,
              schema.format,
              schema.toRawJsonSchemaProperty(),
              definitions
            )
          }
          PossiblePropertyV7.OfEmpty -> PropertyTypes.Property.AnyProperty
        }
        JSONSchemaProperty(
          propertyName = name,
          type = propType
        )
      },
      additionalProperties = if (addProps == null) Option.None() else Option.Some(addProps),
      patternProperties = patternProps,
      required = when (this.required) {
        is Required.RequiredReference -> PropertyTypes.Property.Required.Reference(this.required.value.ref!!)
        is Required.RequiredStrings -> PropertyTypes.Property.Required.ListOfStrings(this.required.value)
        Required.Empty -> PropertyTypes.Property.Required.ListOfStrings(listOf())
      },
      dependencies = this.dependencies,
      definitions = definitions
    )
  }

  override suspend fun buildJSONArraySchema(): JSONSchemaArray {
    TODO("Not yet implemented")
  }
}
