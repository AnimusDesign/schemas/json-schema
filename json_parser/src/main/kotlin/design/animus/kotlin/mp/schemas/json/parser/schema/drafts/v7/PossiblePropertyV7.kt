/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.drafts.v7

import design.animus.kotlin.mp.schemas.json.parser.logger
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class PossiblePropertyV7 {
  @Serializable
  data class OfBoolean(val item: Boolean) : PossiblePropertyV7()

  @Serializable
  data class OfProperty(val item: RawJSONSchemaPropertyV7) : PossiblePropertyV7()

  @Serializable
  object OfEmpty : PossiblePropertyV7()

  @Serializer(forClass = PossiblePropertyV7::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<PossiblePropertyV7> {
    override fun serialize(encoder: Encoder, obj: PossiblePropertyV7) {
      when (obj) {
        is OfBoolean -> encoder.encodeBoolean(obj.item)
        is OfProperty -> encoder.encodeSerializableValue(RawJSONSchemaPropertyV7.serializer(), obj.item)
        OfEmpty -> encoder.encodeNull()
      }
    }

    override fun deserialize(decoder: Decoder): PossiblePropertyV7 = try {
      logger.debug { "In RawProperty properties deserialize" }
      val possibleOfProperty = try {
        OfProperty(decoder.decodeSerializableValue(RawJSONSchemaPropertyV7.serializer()))
      } catch (e: Exception) {
        logger.debug { "Raw Property is not of json schema property" }
        null
      }
      val possibleOfBoolean = try {
        OfBoolean(decoder.decodeBoolean())
      } catch (e: Exception) {
        logger.debug { "Raw Property is not of boolean property" }
        null
      }
      possibleOfProperty ?: possibleOfBoolean ?: OfEmpty
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in RawProperty: ${e.message}" }
      OfEmpty
    }
  }
}
