/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import design.animus.kotlin.contract.Versions.Dependencies

val artifactID = "json_parser"
kotlin {
  target {
    mavenPublication {
      artifactId = artifactID
    }
  }
  sourceSets {
    main {
      dependencies {
        api(project(":json_core:"))
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        api("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        api("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        api("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      resources.setSrcDirs(listOf("src/test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/src/test/resources")
  into("$buildDir/classes/kotlin/test")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
  kotlinOptions.apiVersion = "1.4"
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      this.groupId = group.toString()
      this.version = version
      artifactId = artifactID
      from(components["java"])
    }
  }
}

tasks.withType<Test>().configureEach {
  useJUnit()
  reports.junitXml.isEnabled = true
}
