/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
sealed class Required {
  @Serializable
  data class RequiredReference(val value: RawJSONSchemaProperty) : Required()

  @Serializable
  data class RequiredStrings(val value: List<String>) : Required()

  @Serializable
  object Empty : Required()

  @Serializer(forClass = Required::class)
  @ExperimentalSerializationApi
  companion object : KSerializer<Required> {
    override fun serialize(encoder: Encoder, value: Required) {
      when (value) {
        is Empty -> encoder.encodeNull()
        is RequiredStrings -> encoder.encodeSerializableValue(ListSerializer(String.serializer()), value.value)
        is RequiredReference -> encoder.encodeString(value.value.ref!!)
      }
    }

    override fun deserialize(decoder: Decoder): Required = try {
      logger.debug { "In Required deserializer for" }
      val possibleReference = try {
        RequiredReference(decoder.decodeSerializableValue(RawJSONSchemaProperty.serializer()))
      } catch (e: Exception) {
        logger.debug { "Not String Reference" }
        null
      }
      val possibleListStrings = try {
        RequiredStrings(decoder.decodeSerializableValue(ListSerializer(String.serializer())))
      } catch (e: Exception) {
        logger.debug { "Not list of strings." }
        null
      }
      possibleReference ?: possibleListStrings ?: Empty
    } catch (e: Exception) {
      logger.error { "Encountered an unexpected exception in Required: ${e.message}" }
      Empty
    }
  }
}
