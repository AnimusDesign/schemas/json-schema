/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.unions

import design.animus.kotlin.mp.schemas.json.parser.schema.records.raw.RawJSONSchemaProperty
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.json.parser.schema.utils.PropertyUtils

suspend fun Map<String, RawJSONSchemaProperty>.castToPropertyType(definitions: Definitions): Map<Regex, PropertyTypes> {
  return this.map { (pattern, patternSchema) ->
    val patternType = when (patternSchema.type) {
      null -> PropertyUtils.getSingleType(pattern, patternSchema.format, patternSchema, definitions)
      else -> PropertyUtils.getPropertyType(patternSchema.type, patternSchema.format, patternSchema, definitions)
    }
    Pair(Regex(pattern), patternType)
  }.toMap()
}
