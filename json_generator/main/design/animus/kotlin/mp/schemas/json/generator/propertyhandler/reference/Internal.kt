/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.propertyhandler.reference

import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.containers.createObjectSerializerName
import design.animus.kotlin.mp.schemas.json.generator.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.serialization.Serializable
import kotlin.system.exitProcess
import kotlinx.serialization.json.JsonElement

suspend fun internalReferenceHandler(args: PropertyToClassArgs): TypeResponse {
  val prop = args.prop as PropertyTypes.Property.Reference
  val config = args.config
  return try {
    val refHierarchy = prop.reference.replace("#/", "").split("/")
    logger.debug { "Reference hierarchy is: $refHierarchy" }
    if (prop.reference == "#") {
      logger.debug { "Found a self reference responding with a package:${config.packageBase} and name:${config.schemaName}" }
      TypeResponse.Name(ClassName(config.packageBase, config.schemaName))
    } else if (refHierarchy[0] == "definitions" || refHierarchy.contains("schemas")) {
      val refPackage =
        mutatePackageNameIfEnabled("${config.packageBase}.definitions", refHierarchy.last(), config)
      val (refClass, _, _) = mutateNameIfEnabled(refHierarchy.last().trim(), config)
      logger.debug { "Received a definition reference of ${prop.reference} cast to ClassName($refPackage, $refClass)" }
      // This is necessary as TypeResponse.Name is an Option of KClass
      // Which we can't access here.
      val propDefinition = jsonSchema.getReference(refHierarchy.last())
      val className = if (jsonSchema.definitionRequiresSerializer(refHierarchy.last())) {
        logger.debug { "Reference type requires a custom serializer annotating." }
        ClassName(refPackage, refClass)
          .copy(
            annotations = listOf(
              AnnotationSpec.builder(Serializable::class)
                .addMember(
                  "with=%T::class",
                  ClassName(refPackage, createObjectSerializerName(refClass))
                )
                .build()
            )
          )
      } else {
        logger.debug { "Reference type does NOT require a custom serializer" }
        ClassName(refPackage, refClass)
      }
      TypeResponse.Name(className, definition = Option.Some(propDefinition))
    } else {
      logger.warn { "Failed to determine type of reference: $refHierarchy, this should be fixed." }
      TypeResponse.Name<Unit>(JsonElement::class.toClassName())
    }
  } catch (e: Exception) {
    logger.error(e) { "Failed to determine type of reference for ${prop.reference}. " }
    e.printStackTrace()
    exitProcess(1)
  }
}
