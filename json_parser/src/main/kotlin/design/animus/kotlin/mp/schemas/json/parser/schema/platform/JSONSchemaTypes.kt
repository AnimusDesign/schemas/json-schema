/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.parser.schema.platform

import com.benasher44.uuid.Uuid
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.core.serializers.*
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.serialization.json.JsonElement

enum class JSONSchemaTypes(
  val typeName: String,
  val format: String?,
  val platformRepresentation: PlatformRepresentation<*>
) {
  Number(
    "number",
    null,
    PlatformRepresentation.common(
      kotlin.Number::class.toClassRepresentation(
        Option.Some(
          NumberSerializer::class
        )
      )
    )
  ),
  Float(
    "number",
    "float",
    PlatformRepresentation.common(
      kotlin.Float::class.toClassRepresentation()
    )
  ),
  Double(
    "number",
    "double",
    PlatformRepresentation.common(
      kotlin.Double::class.toClassRepresentation()
    )
  ),
  Int(
    "integer",
    null,
    PlatformRepresentation.common(
      kotlin.Int::class.toClassRepresentation()
    )
  ),
  Short(
    "integer",
    "int32",
    PlatformRepresentation.common(
      kotlin.Short::class.toClassRepresentation()
    )
  ),
  Long(
    "integer",
    "int64",
    PlatformRepresentation.common(
      kotlin.Long::class.toClassRepresentation()
    )
  ),
  PlainString(
    "string",
    null,
    PlatformRepresentation.common(
      kotlin.String::class.toClassRepresentation()
    )
  ),
  Date(
    "string",
    "date",
    PlatformRepresentation.common(
      LocalDate::class.toClassRepresentation(
        Option.Some(
          LocalDateSerializer::class
        )
      )
    )
  ),
  DateTime(
    "string",
    "date-time",
    PlatformRepresentation.common(
      Instant::class.toClassRepresentation(
        Option.Some(
          DateTimeSerializer::class
        )
      )

    )
  ),
  Time(
    "string",
    "date-time",
    PlatformRepresentation.common(
      design.animus.kotlin.mp.schemas.json.core.Time::class.toClassRepresentation(
        Option.Some(
          TimeSerializer::class
        )
      )
    )
  ),
  Password(
    "string",
    "password",
    PlatformRepresentation.common(
      kotlin.String::class.toClassRepresentation()
    )
  ),
  Byte(
    "string",
    "byte",
    PlatformRepresentation.common(
      kotlin.Byte::class.toClassRepresentation()
    )
  ),
  Binary(
    "string",
    "binary",
    PlatformRepresentation.common(
      kotlin.String::class.toClassRepresentation()
    )
  ),
  Email(
    "string",
    "email",
    PlatformRepresentation.common(
      kotlin.String::class.toClassRepresentation()
    )
  ),
  REGEX(
    "string",
    "regex",
    PlatformRepresentation.common(
      Regex::class.toClassRepresentation(
        Option.Some(
          RegexSerializer::class
        )
      )
    )
  ),
  UUID(
    "string",
    "uuid",
    PlatformRepresentation.common(
      Uuid::class.toClassRepresentation(
        Option.Some(
          UuidSerializer::class
        )
      )
    )
  ),
  URI(
    "string",
    "uri",
    PlatformRepresentation.common(
      design.animus.kotlin.mp.schemas.json.core.aliases.URI::class.toClassRepresentation()
    )
  ),
  URIReference(
    "string",
    "uri-reference",
    PlatformRepresentation.common(
      design.animus.kotlin.mp.schemas.json.core.aliases.URIReference::class.toClassRepresentation()
    )
  ),
  URL(
    "string",
    "url",
    PlatformRepresentation.common(String::class.toClassRepresentation())
  ),
  IRI(
    "string",
    "iri",
    PlatformRepresentation.common(
      String::class.toClassRepresentation()
    )
  ),
  IRIReference(
    "string",
    "iri-reference",
    PlatformRepresentation.common(
      String::class.toClassRepresentation()
    )
  ),
  HostName(
    "string",
    "hostname",
    PlatformRepresentation.common(
      String::class.toClassRepresentation()
    )
  ),
  IP4(
    "string",
    "ipv4",
    PlatformRepresentation.common(
      String::class.toClassRepresentation()
    )
  ),
  IP6(
    "string",
    "ipv6",
    PlatformRepresentation.common(
      String::class.toClassRepresentation()
    )
  ),
  Boolean(
    "boolean",
    null,
    PlatformRepresentation.common(
      kotlin.Boolean::class.toClassRepresentation()
    )
  ),
  IntOrString(
    "string",
    "int-or-string",
    PlatformRepresentation.common(
      kotlin.Int::class.toClassRepresentation()
    )
  ),
  Null(
    "null",
    "null",
    PlatformRepresentation.common(JsonElement::class.toClassRepresentation())
  ),
  Duration(
    "string",
    "duration",
    PlatformRepresentation.common(String::class.toClassRepresentation())
  )
}
