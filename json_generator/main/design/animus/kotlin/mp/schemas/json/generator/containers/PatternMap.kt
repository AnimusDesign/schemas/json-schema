/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator.containers

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBaseWithPatternProperties
import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBaseWithPatternProperty
import design.animus.kotlin.mp.schemas.json.core.interfaces.IPatternMap
import design.animus.kotlin.mp.schemas.json.core.interfaces.IPatternMapUtils
import design.animus.kotlin.mp.schemas.json.core.serializers.APatternMapSerializer
import design.animus.kotlin.mp.schemas.json.core.serializers.RegexSerializer
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.json.generator.logger
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable

data class PatternMapResponse(
  val className: TypeName,
  val isSealed: Boolean,
  val interfaceParamterType: TypeName
)

private fun createPrefix(namePrefix: Option<String>) = when (namePrefix) {
  is Option.Some -> namePrefix.item
  is Option.None -> ""
}

private fun createPackageName(config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig) =
  "${config.packageBase}.patternmaps"

private fun createParentClassName(name: String) = "${name.capitalize()}WrapperMap"
private fun createMapName(
  prefix: String,
  name: String,
  index: Int,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
): String = runBlocking {
  val (finalName, _, _) = mutateNameIfEnabled("${prefix.capitalize()}${name.capitalize()}${index}Map", config)
  finalName
}

private fun createRegexPropName(mapName: String) = "${mapName}Regex"

fun createPatternMapTypeMap(
  name: String,
  patterns: Map<Regex, PropertyTypes>,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
): Pair<String, Pair<String, List<String>>> {
  val prefix = createPrefix(namePrefix)
  val finalPackageName = createPackageName(config)
  val parentClassName = createParentClassName(name)
  val regexImportList = mutableListOf<String>()
  val typeMap = patterns.keys.mapIndexed { index, regex ->
    val mapName = createMapName(prefix, name, index, config)
    val regexPropName = createRegexPropName(mapName)
    regexImportList.add(regexPropName)
    "$regexPropName to \"$finalPackageName.$parentClassName.$mapName\""
  }.joinToString(",\n")
  return Pair("mapOf($typeMap)", Pair(finalPackageName, regexImportList.toList()))
}

suspend fun createPatternMap(
  name: String,
  patterns: Map<Regex, PropertyTypes>,
  config: design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig,
  namePrefix: Option<String> = Option.None()
): PatternMapResponse = try {
  logger.debug { "Creating pattern map: $name" }
  val prefix = createPrefix(namePrefix)
  val preName = "${prefix.capitalize()}$name"
  logger.debug { "Initial name is: $preName" }
  val (finalName, _, _) = mutateNameIfEnabled(preName, config)
  logger.debug { "Sanitized to: $finalName " }
  val finalPackageName = createPackageName(config)
  val isSealed = patterns.keys.size > 1
  val parentClassName = createParentClassName(name)
  val parentClassRef = ClassName(finalPackageName, parentClassName)
  val parentClass = TypeSpec.classBuilder(parentClassName)
    .addModifiers(KModifier.SEALED)
    .addAnnotation(Serializable::class)
    .addAnnotation(experimentalSerializationClass)
  val file = if (isSealed) {
    FileSpec.builder(
      finalPackageName,
      parentClassName
    )
  } else {
    FileSpec.builder(
      finalPackageName,
      "PatternMap$finalName"
    )
  }
  val iPatternMap = IPatternMapUtils::class.toImportStatement()
  val iPatternMapProp = IJSONSchemaObjectBaseWithPatternProperty::class.toImportStatement()
  val iPatternMapProps = IJSONSchemaObjectBaseWithPatternProperties::class.toImportStatement()
  val regexSerializer = RegexSerializer::class.toImportStatement()
  file
    .addImport(regexSerializer.first, regexSerializer.second)
    .addImport(iPatternMap.first, iPatternMap.second)
    .addImport(iPatternMapProp.first, iPatternMap.second)
    .addImport(iPatternMapProps.first, iPatternMapProps.second)
  val parentInterface = IPatternMap::class.toClassName()
  val mapRefs = patterns.keys.mapIndexed { index, regex ->
    val prop = patterns[regex] ?: error("Could not find regex: $regex in patterns map.")
    val rawType = propertyToClass(name, prop, config, namePrefix = namePrefix)
      .toTypeName()
    val mapType = ClassName("kotlin.collections", "Map")
      .parameterizedBy(
        ClassName("kotlin", "String"),
        rawType
      )
    val mapName = createMapName(prefix, name, index, config)
    val mapRef = if (isSealed) {
      ClassName("$finalPackageName.$parentClassName", mapName)
    } else {
      ClassName(finalPackageName, mapName)
    }
    val regexPropName = "${mapName}Regex"
    file.addProperty(
      PropertySpec.builder(regexPropName, Regex::class)
        .initializer(
          "Regex(\"%L\")",
          regex.pattern
            .replace("""\""", """\\""")
            .replace("""$""", """\$""")

        )
        .build()
    )
    val regexCompanion = TypeSpec.companionObjectBuilder()
      .addSuperinterface(
        IPatternMapUtils::class.toClassName().parameterizedBy(rawType)
      )
      .addFunction(
        FunSpec.builder("of")
          .addModifiers(KModifier.OVERRIDE)
          .addParameter(
            ParameterSpec.builder(
              "pairs",
              ClassName("kotlin", "Pair")
                .parameterizedBy(
                  ClassName("kotlin", "String"),
                  rawType
                )
            )
              .addModifiers(KModifier.VARARG)
              .build()
          )

          .returns(mapRef)
          .addCode(
            """
                            val map = $mapName(mapOf(*validatePairs($regexPropName, *pairs)))
                            return map
            """.trimIndent()
          )
          .build()
      )
      .build()
    val regexMap = TypeSpec.classBuilder(mapName)
      .addSuperinterface(parentInterface.parameterizedBy(rawType))
      .addModifiers(KModifier.DATA)
      .addAnnotation(Serializable::class)
      .addAnnotation(experimentalSerializationClass)
      .addKdoc(
        """
                Pattern map based on regex ${regex.pattern} target of $rawType
        """.trimIndent()
      )
      .primaryConstructor(
        FunSpec.constructorBuilder()
          .addParameter(
            ParameterSpec.builder("map", mapType)
              .build()
          )
          .addParameter(
            ParameterSpec.builder("regex", Regex::class)
              .defaultValue("%L", regexPropName)
              .build()
          )
          .build()
      )
      .addProperty(
        PropertySpec.builder("map", mapType)
          .initializer("map")
          .addModifiers(KModifier.OVERRIDE)
          .build()
      )
      .addProperty(
        PropertySpec.builder("regex", Regex::class)
          .addModifiers(KModifier.OVERRIDE)
          .addAnnotation(
            AnnotationSpec.builder(Serializable::class)
              .addMember("with=%L", "RegexSerializer::class")
              .build()
          )
          .initializer("regex")
          .build()
      )
      .addType(regexCompanion)
    if (isSealed) regexMap.superclass(parentClassRef)
    val finalRegexMap = regexMap.build()
    Triple(mapRef, finalRegexMap, rawType)
  }
  if (isSealed) {
    val finalParent = parentClass
      .addTypes(
        mapRefs.map { it.second }
      )
      .build()
    file
      .addType(finalParent)
      .build()
      .writeTo(config.outputPath)
    PatternMapResponse(Set::class.toClassName().parameterizedBy(parentClassRef), true, parentClassRef)
  } else {
    val (mapRef, mapType, innerType) = mapRefs.first()
    val mapName = mapRef.simpleName
    val serializerName = createObjectSerializerName(mapName)
    val mapSerializer = TypeSpec.objectBuilder(serializerName)
      .superclass(APatternMapSerializer::class.toClassName().parameterizedBy(mapRef))
      .addSuperclassConstructorParameter("$mapName.serializer()")
      .addAnnotation(experimentalSerializationClass)
      .addProperty(
        PropertySpec.builder("regex", Regex::class)
          .initializer(createRegexPropName(mapName))
          .addModifiers(KModifier.OVERRIDE)
          .build()
      )
      .build()
    file
      .addType(mapType)
      .addType(mapSerializer)
      .build()
      .writeTo(config.outputPath)
    PatternMapResponse(
      mapRef.copy(
        annotations = listOf(
          AnnotationSpec.builder(Serializable::class)
            .addMember("with=%T::class", ClassName(finalPackageName, serializerName))
            .build()
        )
      ),
      false,
      innerType
    )
  }
} catch (e: Exception) {
  logger.error { "Encountered an unexpected excetion while creating pattern map: ${e.message}" }
  e.printStackTrace()
  throw e
}
