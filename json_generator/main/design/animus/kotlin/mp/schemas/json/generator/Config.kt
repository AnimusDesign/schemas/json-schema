/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.generator

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.utils.Platforms
import java.io.File
import kotlinx.serialization.json.JsonElement

data class RenameContext(val packageName: String, val itemName: String)

typealias RenameContextFunction = (design.animus.kotlin.mp.schemas.json.generator.RenameContext) -> String
typealias MutateSchemaFunction = (JsonElement) -> JsonElement

data class GeneratorConfig(
  val packageBase: String,
  val schemaName: String,
  val outputPath: File,
  val schemaFile: File,
  val createBaseObject: Boolean = true,
  val mutateObjectName: Option<design.animus.kotlin.mp.schemas.json.generator.RenameContextFunction> = Option.None(),
  val mutatePackageName: Option<design.animus.kotlin.mp.schemas.json.generator.RenameContextFunction> = Option.None(),
  val mutateSchema: Option<design.animus.kotlin.mp.schemas.json.generator.MutateSchemaFunction> = Option.None(),
  val platforms: Set<Platforms> = setOf(Platforms.Common),
  val createDSL: Boolean = false
)

open class JsonSchemaGeneratorConfig(
  var schema: List<design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig>
)
