package design.animus.kotlin.mp.schemas.json.diff

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaObjectBase
import kotlinx.serialization.json.*
import mu.KotlinLogging

internal val logger = KotlinLogging.logger {}
typealias ChangePath = String

suspend fun ChangePath.extendPath(extension: ChangePath): ChangePath {
  return if (this.isEmpty()) {
    extension
  } else {
    "$this/$extension"
  }
}

suspend fun <A : IJSONSchemaObjectBase> diffJsonObjects(objA: A, objB: A): List<Changes<String?>>? {
  val objAasJsonElement = Json.parseToJsonElement(
    Json.encodeToString(objA.getAJSONSchemaSerializer(), objA)
  )
  logger.debug { "Received a json element A of: $objAasJsonElement" }
  val objBasJsonElement = Json.parseToJsonElement(
    Json.encodeToString(objB.getAJSONSchemaSerializer(), objB)
  )
  logger.debug { "Received a json element B of: $objBasJsonElement" }
  return directDiff(objAasJsonElement, objBasJsonElement)
}

suspend fun directDiff(old: JsonElement?, new: JsonElement?, path: ChangePath = ""): List<Changes<String?>> {
  return when (old) {
    is JsonArray -> diffJsonArray(old, new as JsonArray?, path)
    is JsonObject -> diffJsonObject(old, new as JsonObject?, path)
    is JsonPrimitive -> diffJsonPrimitive(old, new as JsonPrimitive?, path)
    null -> when (new) {
      is JsonArray -> diffJsonArray(old, new as JsonArray?, path)
      is JsonObject -> diffJsonObject(old, new as JsonObject?, path)
      is JsonPrimitive -> diffJsonPrimitive(old, new as? JsonPrimitive?, path)
      null -> listOf()
    }
  }
}

suspend fun diffJsonArray(old: JsonArray?, new: JsonArray?, path: ChangePath = ""): List<Changes<String?>> {
  return when {
    old?.size == new?.size -> {
      old?.mapIndexed { idx, item -> directDiff(item, new?.get(idx), path.extendPath("index:$idx")) }?.flatten()
        ?: listOf()
    }
    new?.size ?: 0 > old?.size ?: 0 -> {
      new?.mapIndexed { idx, item ->
        directDiff(
          if (old?.isEmpty() ?: true) null else old?.get(idx),
          item,
          path.extendPath("index:$idx")
        )
      }?.flatten()
        ?: listOf()
    }
    old?.size ?: 0 > new?.size ?: 0 -> {
      old?.mapIndexed { idx, item ->
        directDiff(
          item,
          if (new?.isEmpty() ?: true) null else new?.get(idx),
          path.extendPath("index:$idx")
        )
      }?.flatten() ?: listOf()
    }
    else -> {
      listOf<Changes<String?>>()
    }
  }
}

suspend fun diffJsonPrimitive(old: JsonPrimitive?, new: JsonPrimitive?, path: ChangePath = ""): List<Changes<String?>> {
  return if (old == new) {
    listOf()
  } else if ((old?.contentOrNull != null) && ((new == null) || (new.contentOrNull == null))) {
    listOf(
      Changes(
        action = Actions.Delete,
        path = path,
        old = old.content,
        new = null
      )
    )
  } else if ((old?.contentOrNull == null) && (new?.contentOrNull != null)) {
    listOf(
      Changes(
        action = Actions.Create,
        path = path,
        old = null,
        new = new.content
      )
    )
  } else if ((old?.contentOrNull != null) && (new?.contentOrNull != null)) {
    listOf(
      Changes(
        action = Actions.Update,
        path = path,
        old = old.content,
        new = new.content
      )
    )
  } else {
    listOf()
  }
}

suspend fun diffJsonObject(old: JsonObject?, new: JsonObject?, path: ChangePath = ""): List<Changes<String?>> {
  return if (old == new) {
    listOf()
  } else {
    val oldKeys = old?.keys ?: setOf<String>()
    val newKeys = new?.keys ?: setOf<String>()
    val keys = oldKeys + newKeys
    val x = keys.flatMap { k ->
      directDiff(old?.get(k), new?.get(k), path.extendPath(k))
    }
    x
  }
}
