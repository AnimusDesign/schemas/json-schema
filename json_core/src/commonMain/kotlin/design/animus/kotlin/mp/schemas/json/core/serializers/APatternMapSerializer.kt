/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core.serializers

import design.animus.kotlin.mp.schemas.json.core.interfaces.IPatternMap
import design.animus.kotlin.mp.schemas.json.core.logger
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.*

@Suppress("UNCHECKED_CAST")
abstract class APatternMapSerializer<O : IPatternMap<*>>(private val serializer: KSerializer<O>) : KSerializer<O> {
  abstract val regex: Regex

  override val descriptor = buildClassSerialDescriptor("APatternMapSerializer")

  private fun makeMapNonNull(map: Map<String, JsonElement?>): Map<String, JsonElement> {
    return map.filter { (_, value) -> value != null }.map { (k, v) -> k to v!! }.toMap()
  }

  override fun deserialize(decoder: Decoder): O {
    logger.debug { "In Decoder for ${this::class.simpleName}" }
    val input = decoder as? JsonDecoder ?: throw SerializationException("Expected Json Input")
    logger.debug { "Input of: $input" }
    val rawObj = input.decodeJsonElement() as? JsonObject ?: throw SerializationException("Expected JsonArray")
    logger.debug { "Cast to raw object: $rawObj" }
    if (rawObj.containsKey("map")) {
      val patternProps = (rawObj["map"] as Map<String, *>).keys.filter { k ->
        regex.containsMatchIn(k)
      }
      logger.debug { "Found pattern props of: patternProps " }
      logger.debug { "Target is instance of pattern property" }
      val patternPropsMap = patternProps.map { k -> k to (rawObj["map"] as JsonObject)[k] }.toMap()
      val map = mapOf(
        "map" to JsonObject(
          makeMapNonNull(patternPropsMap)
        ),
        "regex" to JsonPrimitive(regex.pattern)
      )
      val finalObj = JsonObject(
        makeMapNonNull(map)
      )
      logger.debug { "Built a final object of: $finalObj" }
      val rsp = Json.decodeFromJsonElement(serializer, finalObj)
      return rsp
    } else {
      throw SerializationException("Received empty pattern map.")
    }
  }

  override fun serialize(encoder: Encoder, value: O) {
    logger.debug { "Attempting to encode value: $value of ${value::class.simpleName}" }
    val raw = Json.encodeToJsonElement(serializer, value) as?
      JsonObject ?: throw SerializationException("Expected Object")
    encoder.encodeSerializableValue(JsonObject.serializer(), raw["map"]?.jsonObject!!)
  }
}
