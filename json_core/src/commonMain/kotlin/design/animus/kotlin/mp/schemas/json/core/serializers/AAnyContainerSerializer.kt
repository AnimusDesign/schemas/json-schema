/*
 *  Copyright (C) 2020 Double Shot Software, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package design.animus.kotlin.mp.schemas.json.core.serializers

import design.animus.kotlin.mp.schemas.json.core.interfaces.IJSONSchemaAnyContainer
import design.animus.kotlin.mp.schemas.json.core.logger
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonElement

@ExperimentalSerializationApi
abstract class AAnyContainerSerializer<S : IJSONSchemaAnyContainer>(private val serializer: KSerializer<S>) :
  KSerializer<S> {
  override val descriptor: SerialDescriptor =
    PrimitiveSerialDescriptor("AAnyContainerSerializer", PrimitiveKind.STRING)

  override fun deserialize(decoder: Decoder): S {
    logger.debug { "In Decoder for ${this::class.simpleName}" }
    val input = decoder as? JsonDecoder ?: throw SerializationException("Expected Json Input")
    val rawElement =
      input.decodeJsonElement() as? JsonElement ?: throw SerializationException("Expected Json Element")
    logger.debug { "Cast to raw object: $rawElement" }
    return decoder.decodeSerializableValue(serializer)
  }

  override fun serialize(encoder: Encoder, value: S) {
    encoder.encodeSerializableValue(JsonElement.serializer(), value.items)
  }
}
