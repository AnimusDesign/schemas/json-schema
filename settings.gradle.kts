import java.net.URI

enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "json"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("1.4.30")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
                // Update Version Build Source if being changed.
                useVersion("1.4.32")
            }
            if (
                requested.id.id.startsWith("design.animus.kotlin.mp.schemas.json.generator")
            ) {
              useVersion("0.0.3-SNAPSHOT")
            }
            if (
                requested.id.id.startsWith("org.gradle.kotlin.kotlin-dsl")
            ) {
                useVersion("2.0.0")
            }
            if (requested.id.id.startsWith("net.researchgate.release")) {
                // Update Version Build Source if being changed.
                useVersion("2.8.1")
            }

        }
    }
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
        gradlePluginPortal()
        maven { url = uri("https://animusdesign-repository.appspot.com") }
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }
        maven { url = java.net.URI("https://kotlin.bintray.com/kotlin-dev") }
    }
}

include(":json_core:")
include(":json_diff:")
include(":json_parser:")
include(":json_generator:")
